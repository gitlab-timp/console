[vi_gitlab_notes]: ./imgs/vi_gitlab_notes.png "GitLab time notes"
[vi_gitlab_time_tracking]: ./imgs/vi_gitlab_time_tracking.png "GitLab time tracking"
[vi_dashboard]: ./imgs/vi_dashboard.png "Dashboard"
[vi_dashboard_2]: ./imgs/vi_dashboard_2.png "Dashboard extended"
[vi_my_work]: ./imgs/vi_my_work.png "My work"
[vi_login_token]: ./imgs/vi_login_token.png "Access Token login"
[vi_project_labels_by_users]: ./imgs/vi_project_labels_by_users.png "Report - Project labels by users"
[vi_project_issue_per_user]: ./imgs/vi_project_issue_per_user.png "Report - Project issues by user"
[vi_by_user_on_a_project]: ./imgs/vi_by_user_on_a_project.png "Report - Time by user on a project"

# Timp

**Timp** est un projet de visualisation réalisé dans le cadre du cours de *VI* du Master de la HES-SO.

Réalisé par Joao Mendes, David Wittwer et Fabio Vitali.

## Description

Le but de ce projet, est de fournir une visualisation des temps loggés pour une tâche sur un serveur **GitLab** 
(actuellement seul le serveur https://gitlab.com est pris en charge). 

## État de l'art

*GitLab* offre une visualisation des temps loggés sur ses serveurs, mais cette dernière n'est absolument pas **user 
friendly**.

![alt text][vi_gitlab_notes]

L'image ci-dessus est la visualisation que l'on a pour savoir quel utilisateur à loggé du temps dans une tâche. 
Le problème c'est que pour y avoir accès on doit se trouver dans la page de la tâche en question.

L'image ci-dessous permet quant à elle de visualiser le temps total loggé pour une tâche par tous les utilisateurs et de 
mesurer l'écart avec le temps qui avait été estimé.

![alt text][vi_gitlab_time_tracking]

## Travail réalisé

Pour ce projet, nous avons décidé de créér une application web ([Angular](https://angular.io/)) pour la visualisation de ces temps et d'offrir 
différentes pages ou chaque page offre un type de visualisation différente.

### Chargement et traitement des données.

Les données de ce projet sont celles fournies directement par l'[API *GitLab*](https://docs.gitlab.com/ee/API/). 
Cependant, certaines données mises à dispostion doivent être transformées avant de pouvoir être utilisées. L'accès aux 
temps loggés par chaque utilisateur est possible en parsant la chaine de caractères que l'on retrouve dans la 
visualisation ci-dessous:

![alt text][vi_gitlab_notes]

Aussi les filtres fournis dans l'API ne permettent pas un accès facile à certaines données, du coup pour chaque page on 
est obligés de charger préalablement toutes les données nécessaires avant d'afficher les visualisations que l'on a 
créées. Dernier point et pas des moindres, certaines requêttes effectuées par l'application risquent d'échouer en 
fonction des droits de l'utilisateur qui l'utilise.

Dans notre cas nous avons rencontré, un problème au chargement des données des merge requests, qui entraine un plantage 
de tous les chargements, c'est pourquoi nous avons décidé de ne pas charger les données relatives au merge requests et 
de travailler uniquement avec les issues, le temps que l'on trouve une solution au problème.

#### Création de l'accès à l'API

Pour pouvoir utiliser l'API de *GitLab*, il faut préalablement générer un jeton d'accès depuis la page 
**Settings - Access Tokens** du serveur *GitLab* avec le scope *Api*.

### Page - Login

La page login permet de renseigner le token qui sera utilisé pour la session en cours. Le token permettra d'identifier 
l'utilisateur qui utilise l'application.

![alt text][vi_login_token]

Ce jeton sera stocké dans le local storage de l'application et sera utilisé pour toutes les requêttes qui sont
 effectuées par l'application.

### Page - Dashboard

La page *Dashboard* permet de voir quels sont les **Issues** ainsi que les **Merge requests** qui ont été assignées à 
l'utilisateur connecté.

![alt text][vi_dashboard]

Lorsque l'on clique sur une **Issue** ou **Merge request** on obtient une vue plus détaillée de cette dernière.

![alt text][vi_dashboard_2]

### Page - My work

La page *My work* permet de voir dans une semaine tous les temps loggés pour toutes les **Issues** et **Merge requests**.
Un code couleur permet d'identifier facilement de quel type de log il s'agit.

![alt text][vi_my_work]

Cette page a été créée afin de reproduire celle de [Tempo](https://www.tempo.io/fr/), l'outil de gestion de temps de 
[Jira](https://fr.atlassian.com/software/jira).

Elle permet une visualisation rapide du temps total loggé par jour ainsi que pour chaque tâche du jour. A terme on 
aimerais offrir plus de fonctionnalités sur cette page comme la possibilité d'avoir plus de détail en cliquant sur une 
tâche ainsi que la possibilité de saisir un message de **Work log** et la possibilité d'exporter ces données.

### Page - Reports

La page *Reports* offre différentes visualisation accéssibles par un selecteur. Chaque visualisation met en évidence le 
temps loggé en fonction d'autres données de *GitLab*. Elle a aussi été conçue afin de pouvoir offrir plus de 
visualisations par la suite.

Dans les graphiques nous avons opté pour utiliser [Patternomaly](https://github.com/ashiguruma/patternomaly) qui permet 
d'avoir des graphiques facilement lisibles aussi bien par les humains avec une vision normale que ceux souffrant de 
daltonisme.

#### Temps loggés pour chaque label par utilisateur

Une des visualisations qui a été mise à disposition, permet de voir le temps loggé par chaque utilisateur dans chaque 
projet en fonction des labels qui ont été attachés à chaque tâche. Ce type de visualisation permet par exemple 
d'identifer si certains utilisateurs sont cantonnés dans un certain type de tâche et de rééquilibrer l'assignement des 
tâches à chacun.

![alt text][vi_project_labels_by_users]

Pour cette visualisation, on peut appliquer un filtre par dates et par label à afficher. Le graphique étant intéractif 
on peut facilement afficher ou cacher les temps par membres. Les valeurs sont les sommes des temps loggés en heures. Le
graphique en barre horizontales a été choisi car après avoir trié les données dans un ordre décroissant en fonction du
temps total passé par un utilisateur cela donne l'illusion d'avoir un classement ce qui est assez parlant.

#### Temps loggés par utilisateurs et par issues

Cette visualisation permet d'avoir un apercu rapide du temps qui été loggé dans les issues d'un projet donné.
On peut ainsi comparer ces issues et un diagramme Pie chart permet de mettre en évidences celles qui ont demandé 
plus de temps par rapport aux autres.

![Affichage des issues par projet][vi_project_issue_per_user]

Ces temps peuvent être filtrés par membre et par date, permettant de voir quels membres ont travaillé sur quelles issues.

#### Temps loggés par utilisateur sur un projet.

Cette visualisation permet d'avoir un apercu rapide du temps passé par chaque utilisateur sur un projet donné.
On peut ainsi comparer facilement le temps passé par chaque contributeur sur un projet. Un Polar chart à été choisi, car
il permet en plus de voir très facilement le nombre de contributeur d'un projet, a condition cependant qu'ils aient tous
loggé un minimum de temps.

![Affichage du temps des contributeur par projet][vi_by_user_on_a_project]

Ces temps peuvent être filtré par membre et par date.

## Conclusion

La résalisation de projet a été très intéressante parce qu'elle nous a obligé à trouver quelles sont les infos que l'on 
aimerais visualiser ainsi qu'à trouver la meilleure visualisation possible pour chaque donnée. De plus, il s'agit d'un 
projet concret qui continuera à évoluer par la suite et sera disponible pour tous les utilisateurs de **GitLab** qui 
cherchent à avoir une meilleure visualisation de leurs temps loggés.
