/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {MergeRequestsListComponent} from './merge-requests-list.component';
import {MaterialModule} from '../../material.module';

@NgModule({
    declarations: [
        MergeRequestsListComponent
    ],
    exports: [
        MergeRequestsListComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        MaterialModule
    ]
})

export class MergeRequestsListModule {
}
