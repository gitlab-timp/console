/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MergeRequestsListComponent} from './merge-requests-list.component';
import {HttpClientModule} from '@angular/common/http';
import {DisplayEnum} from '../../enums/display.enum';
import {MergeRequestStateEnum} from '../../service/git-lab/merge-requests/merge-request-state.enum';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../material.module';

describe('Component - MergeRequestList', () => {
    let component: MergeRequestsListComponent;
    let fixture: ComponentFixture<MergeRequestsListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MergeRequestsListComponent
            ],
            imports: [
                CommonModule,
                HttpClientModule,
                MaterialModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MergeRequestsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`getDetailDisplayMode(null) should return ${DisplayEnum.COLLAPSED}`, () => {
        const displayMode = component.getDetailDisplayMode(null);
        expect(displayMode).toBe(DisplayEnum.COLLAPSED);
    });

    it(`getDetailDisplayMode(dummyData) should return ${DisplayEnum.COLLAPSED}`, () => {
        const dummyData = {
            id: 1,
            iid: 1,
            project_id: 1,
            title: 'Pizza',
            description: '',
            state: MergeRequestStateEnum.OPENED,
            created_at: '',
            updated_at: '',
            target_branch: '',
            source_branch: '',
            upvotes: 1,
            downvotes: 1,
            author: null,
            assignee: null,
            source_project_id: 1,
            target_project_id: 1,
            labels: [],
            work_in_progress: false,
            milestone: null,
            merge_when_pipeline_succeeds: false,
            merge_status: '',
            sha: '',
            merge_commit_sha: '',
            user_notes_count: 1,
            discussion_locked: '',
            should_remove_source_branch: false,
            force_remove_source_branch: false,
            allow_collaboration: false,
            allow_maintainer_to_push: false,
            web_url: '',
            time_stats: null,
            squash: false,
            approvals_before_merge: '',
            project: null,
            labels_full: []
        };

        component.dataExpanded = {
            id: 1,
            iid: 1,
            project_id: 1,
            title: 'Pizza',
            description: '',
            state: MergeRequestStateEnum.OPENED,
            created_at: '',
            updated_at: '',
            target_branch: '',
            source_branch: '',
            upvotes: 1,
            downvotes: 1,
            author: null,
            assignee: null,
            source_project_id: 1,
            target_project_id: 1,
            labels: [],
            work_in_progress: false,
            milestone: null,
            merge_when_pipeline_succeeds: false,
            merge_status: '',
            sha: '',
            merge_commit_sha: '',
            user_notes_count: 1,
            discussion_locked: '',
            should_remove_source_branch: false,
            force_remove_source_branch: false,
            allow_collaboration: false,
            allow_maintainer_to_push: false,
            web_url: '',
            time_stats: null,
            squash: false,
            approvals_before_merge: '',
            project: null,
            labels_full: []
        };

        component.isExpandLoaded = true;

        const expandMode = component.getDetailDisplayMode(dummyData);
        expect(expandMode).toBe(DisplayEnum.EXPANDED);
    });
});

