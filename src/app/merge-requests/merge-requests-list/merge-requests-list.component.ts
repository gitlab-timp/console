/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material';
import {HttpResponse} from '@angular/common/http';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {ObserveEnum} from '../../service/observe.enum';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ProjectsService} from '../../service/git-lab/projects/projects.service';
import 'rxjs/add/observable/forkJoin';
import {NotificationTypeEnum} from '../../enums/notification-type.enum';
import {GitlabPaginationEnum} from '../../service/git-lab/git-lab-pagination.enum';
import {NotificationUtils} from '../../utils/notification.utils';
import 'rxjs-compat/add/operator/expand';
import 'rxjs-compat/add/observable/of';
import 'rxjs-compat/add/operator/map';
import 'rxjs-compat/add/observable/empty';
import {MergeRequest} from '../../entity/merge-request';
import {MergeRequestsService} from '../../service/git-lab/merge-requests/merge-requests.service';
import {MergeRequestScopeEnum} from '../../service/git-lab/merge-requests/merge-request-scope.enum';
import {DisplayEnum} from '../../enums/display.enum';
import {MergeRequestStateEnum} from '../../service/git-lab/merge-requests/merge-request-state.enum';
import {CheckUtils} from '../../utils/check.utils';

@Component({
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
        ])
    ],
    moduleId: module.id,
    selector: 'app-merge-requests-list',
    templateUrl: './merge-requests-list.component.html',
    styleUrls: ['./merge-requests-list.component.css']
})

export class MergeRequestsListComponent implements OnInit {
    public data: Array<MergeRequest>;
    public dataExpanded: MergeRequest;
    public columnsToDisplay: Array<string>;
    public state: MergeRequestStateEnum;
    public totalCount: number;
    public pageSize: number;
    public isLoadingData: boolean;
    public isLoadingDataError: boolean;
    public isLoadingExpand: boolean;
    public isLoadingExpandError: boolean;
    public isExpandLoaded: boolean;

    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private  mergeRequestsService: MergeRequestsService, private projectsService: ProjectsService) {
        this.data = [];
        this.dataExpanded = null;
        this.columnsToDisplay = ['iid', 'title', 'spend', 'estimate'];
        this.state = MergeRequestStateEnum.OPENED;
        this.totalCount = 0;
        this.pageSize = 5;
    }

    ngOnInit() {
        this.loadMergeRequests();
    }

    /**
     * Load data
     *
     * @description Not Tested only display changes, requests tested on service
     */
    public loadMergeRequests() {
        this.dataExpanded = null;
        this.isExpandLoaded = false;
        this.isLoadingExpand = false;
        this.isLoadingExpandError = false;
        this.isLoadingData = true;
        this.isLoadingDataError = false;

        merge(this.paginator.page).pipe(
            startWith({}),
            switchMap(() => {
                this.isLoadingData = true;

                return this.mergeRequestsService.find(
                    this.paginator.pageIndex,
                    {state: this.state, scope: MergeRequestScopeEnum.ASSIGNED_TO_ME},
                    ObserveEnum.RESPONSE,
                    this.pageSize
                );
            }),
            map((res: HttpResponse<Array<MergeRequest>>) => {
                this.isLoadingData = false;
                this.isLoadingDataError = false;
                this.totalCount = Number(res.headers.get(GitlabPaginationEnum.TOTAL_ITEMS));

                return res.body;
            }),
            catchError(() => {
                this.isLoadingData = false;
                this.isLoadingDataError = true;

                return observableOf([]);
            })
        ).subscribe((data: Array<MergeRequest>) => this.data = data);
    }

    /**
     * Load Projet data and Project labels of MergeRequest
     *
     * @description Not Tested only display changes, requests tested on service
     *
     * @param row MergeRequest
     */
    public loadMergeRequestExpandedData(row: MergeRequest) {
        this.isExpandLoaded = false;
        this.isLoadingExpandError = false;
        this.isLoadingData = false;
        this.isLoadingDataError = false;

        if (CheckUtils.isNull(this.dataExpanded) || row.id !== this.dataExpanded.id) {
            this.isLoadingExpand = true;
            this.dataExpanded = null;

            this.loadMergeRequestsNotes(row);

            Observable.forkJoin(
                this.projectsService.get(row.project_id),
                this.projectsService.findLabels(row.project_id)
            ).subscribe((res) => {
                const index = this.data.indexOf(row);
                this.data[index].project = res[0];
                this.data[index].labels_full = res[1].filter(label => row.labels.includes(label.name));

                this.dataExpanded = row;

                this.isExpandLoaded = true;
                this.isLoadingExpand = false;
                this.isLoadingExpandError = false;
            }, (error) => {
                this.isExpandLoaded = false;
                this.isLoadingExpand = false;
                this.isLoadingExpandError = true;
                NotificationUtils.showNotification(NotificationTypeEnum.DANGER, error);
            });
        } else {
            this.dataExpanded = null;
        }
    }

    /**
     * Load Notes of MergeRequest to find who logged time and when
     *
     * @description Not Tested only display changes, requests tested on service
     *
     * @param row MergeRequest
     */
    public loadMergeRequestsNotes(row: MergeRequest) {
        // Will be used to load individual time logs
        // console.log(`Load notes - ${row.title}`);

        // this.mergeRequestsService.findNotesForMergeRequest(row.noteable_id, row.iid, null, Observe.RESPONSE)
        //     .expand((res: HttpResponse<Array<Note>>) => {
        //         const nextPage = res.headers.getAuthenticated(GitlabPagination.NEXT_PAGE);
        //
        //         console.log(GitlabPagination.NEXT_PAGE);
        //         console.log(res);
        //         console.log(nextPage);
        //
        //         if (nextPage !== undefined) {
        //             return;
        //         } else {
        //             return observableOf([]);
        //         }
        //     }).map((res: Response) => res.json());
    }

    /**
     * Get row display mode
     *
     * @param row MergeRequest who user clicked
     */
    public getDetailDisplayMode(row: MergeRequest): string {
        return this.isExpandedRow(row) ? DisplayEnum.EXPANDED : DisplayEnum.COLLAPSED;
    }

    /**
     * Check if row is expanded
     *
     * @param row MergeRequest who user clicked
     */
    public isExpandedRow(row: MergeRequest): boolean {
        return (this.dataExpanded !== null && row.id === this.dataExpanded.id && this.isExpandLoaded);
    }
}
