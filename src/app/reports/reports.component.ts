/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, OnInit} from '@angular/core';
import {LoadingStep} from '../model/loading-step';
import {RawData} from '../model/raw-data';
import {IssuesService} from '../service/git-lab/issues/issues.service';
import {MergeRequestsService} from '../service/git-lab/merge-requests/merge-requests.service';
import {NotesService} from '../service/git-lab/notes/notes.service';
import {ProjectsService} from '../service/git-lab/projects/projects.service';
import {LoadingStatusEnum} from '../enums/loading-status.enum';
import {Observable} from 'rxjs';
import {Report} from '../model/report';

@Component({
    selector: 'app-reports',
    styleUrls: ['./reports.component.css'],
    templateUrl: './reports.component.html'
})

export class ReportsComponent implements OnInit {

    public isLoadingData: boolean;
    public isLoadingDataError: boolean;
    public loadingSteps: Array<LoadingStep>;
    public rawData: RawData;
    public reports: Array<Report>;
    public cReportId: number;

    constructor(
        private issueService: IssuesService,
        private mergeRequestsService: MergeRequestsService,
        private notesService: NotesService,
        private projectsService: ProjectsService
    ) {
        this.initReports();
        this.init();
        this.loadData();
    }

    ngOnInit() {

    }

    /**
     * Set inital value for all data
     */
    private init() {
        this.rawData = new RawData();
        this.cReportId = null;
        this.isLoadingData = false;
        this.isLoadingDataError = false;
        this.resetDatas();
    }

    /**
     * Reset loading steps
     */
    private resetLoadingSteps() {
        this.loadingSteps = [
            new LoadingStep(LoadingStatusEnum.FETCHING_PROJECTS),
            new LoadingStep(LoadingStatusEnum.FETCHING_ISSUES),
            new LoadingStep(LoadingStatusEnum.FETCHING_ISSUES_NOTES),
            new LoadingStep(LoadingStatusEnum.FETCHING_MERGE_REQUESTS),
            new LoadingStep(LoadingStatusEnum.FETCHING_MERGE_REQUESTS_NOTES)
        ];
    }

    /**
     * Reset loading steps
     */
    private initReports() {
        this.cReportId = 0;

        this.reports = [
            new Report(1, 'Time spent by labels of project members'),
            new Report(2, 'Time spent by user on a project'),
            new Report(3, 'Time spent by issue in a project')
        ];
    }

    /**
     * Update loading step done statusMsg
     *
     * @param status Name of the statusMsg
     */
    private loadingStepDone(status: LoadingStatusEnum) {
        const loadingStep = this.loadingSteps.filter((ls: LoadingStep) => {
            return ls.statusMsg === status;
        });

        loadingStep[0].setDone();
    }

    /**
     * Update loading step done statusMsg
     *
     * @param status Name of the statusMsg
     */
    private loadingStepError(status: LoadingStatusEnum) {
        const loadingStep = this.loadingSteps.filter((ls: LoadingStep) => {
            return ls.statusMsg === status;
        });

        loadingStep[0].setError();
    }

    /**
     * Update loading step done statusMsg
     *
     * @param status Name of the statusMsg
     */
    private loadingStepLoading(status: LoadingStatusEnum) {
        const loadingStep = this.loadingSteps.filter((ls: LoadingStep) => {
            return ls.statusMsg === status;
        });

        loadingStep[0].setLoading();
    }

    /**
     * Reset all arrays datas
     */
    private resetDatas() {
        this.isLoadingData = false;
        this.isLoadingDataError = false;
        this.rawData = new RawData();
        this.resetLoadingSteps();
    }

    /**
     * Load all datas to extract time logged
     */
    public loadData() {
        this.resetDatas();
        this.isLoadingData = true;

        this.loadingStepLoading(LoadingStatusEnum.FETCHING_PROJECTS);

        this.projectsService.getAllProjectsPages(this.rawData).subscribe(
            () => {
            },
            (err) => {
                console.log(err);
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_PROJECTS);
            },
            () => {
                console.log(this.rawData);
                this.loadingStepDone(LoadingStatusEnum.FETCHING_PROJECTS);
                this.loadIssues();
            }
        );
    }

    /**
     * Load all issues for all projects
     */
    public loadIssues() {
        this.loadingStepLoading(LoadingStatusEnum.FETCHING_ISSUES);

        const jobs: Array<Observable<any>> = this.rawData.projects.map(item => {
            return this.issueService.getAllIssuesByProject(this.rawData, item.id);
        });

        Observable.forkJoin(jobs).subscribe(
            () => {
            },
            (err) => {
                console.log(err);
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_ISSUES);
            },
            () => {
                console.log(this.rawData);
                this.loadingStepDone(LoadingStatusEnum.FETCHING_ISSUES);
                this.loadIssuesNotes();
            }
        );
    }

    /**
     * Load all notes for all issues
     */
    public loadIssuesNotes() {
        this.loadingStepLoading(LoadingStatusEnum.FETCHING_ISSUES_NOTES);

        const jobs: Array<Observable<any>> = this.rawData.issues.map((item) => {
            return this.notesService.loadIssuesNotesPages(this.rawData, item.project_id, item.iid);
        });

        Observable.forkJoin(jobs).subscribe(
            () => {
            },
            (err) => {
                console.log(err);
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_ISSUES_NOTES);
            },
            () => {
                console.log(this.rawData);
                this.loadingStepDone(LoadingStatusEnum.FETCHING_ISSUES_NOTES);
                // this.loadMergeRequests();
                this.isLoadingData = false;
            }
        );
    }

    /**
     * Load all merge requests for all projects
     */
    public loadMergeRequests() {
        this.loadingStepLoading(LoadingStatusEnum.FETCHING_MERGE_REQUESTS);

        const jobs: Array<Observable<any>> = this.rawData.projects.map((item) => {
            return this.mergeRequestsService.getAllMergeRequestsByProject(this.rawData, item.id);
        });

        Observable.forkJoin(jobs).subscribe(
            () => {
            },
            (err) => {
                console.log(err);
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_MERGE_REQUESTS);
            },
            () => {
                console.log(this.rawData);
                this.loadingStepDone(LoadingStatusEnum.FETCHING_MERGE_REQUESTS);
                this.loadMergeRequestNotes();
            }
        );
    }

    /**
     * Load all notes for all merge requests
     */
    public loadMergeRequestNotes() {
        this.loadingStepLoading(LoadingStatusEnum.FETCHING_MERGE_REQUESTS_NOTES);

        const jobs: Array<Observable<any>> = this.rawData.mergeRequests.map((item) => {
            return this.notesService.loadMergeRequestNotesPages(this.rawData, item.project_id, item.iid);
        });

        Observable.forkJoin(jobs).subscribe(
            () => {
            },
            (err) => {
                console.log(err);
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_MERGE_REQUESTS_NOTES);
            },
            () => {
                console.log(this.rawData);
                this.loadingStepDone(LoadingStatusEnum.FETCHING_MERGE_REQUESTS_NOTES);
                this.isLoadingData = false;
            }
        );
    }

}
