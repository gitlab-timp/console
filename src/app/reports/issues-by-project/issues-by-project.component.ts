/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {RawData} from '../../model/raw-data';
import {MatOption} from '@angular/material';
import {Project} from '../../entity/project';
import {ProjectsService} from '../../service/git-lab/projects/projects.service';
import {IssuesService} from '../../service/git-lab/issues/issues.service';
import {CheckUtils} from '../../utils/check.utils';
import {Observable} from 'rxjs';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Issue} from '../../entity/issue';
import {TimeLog} from '../../model/time-log';
import {ColorUtils} from '../../utils/color.utils';
import {generate} from 'patternomaly';


@Component({
    selector: 'app-issues-by-project',
    templateUrl: './issues-by-project.component.html',
    styleUrls: ['./issues-by-project.component.css']
})
export class IssuesByProjectComponent {

    @ViewChild('allMembers') private allMembers: MatOption;
    @ViewChild('allLabels') private allLabels: MatOption;

    @Input() rawData: RawData;

    public allOption: number;
    public chartDatasets: Array<any>;
    public chartLabels: string[];
    public chartLegend: boolean;
    public chartOptions: any;
    public chartColors: Array<any>;
    public chartType: string;
    public filtersForm: FormGroup;
    public fromDate: Date;
    public listIssues: Array<Issue>;
    public colorBlindEnabled: boolean;
    public hasReport: boolean;
    public project: Project;
    public projectId: number;
    public isComputingData: boolean;
    public isLoadingData: boolean;
    public isLoadingDataError: boolean;
    public toDate: Date;

    constructor(private projectsService: ProjectsService,
        private issuesService: IssuesService,
        private fb: FormBuilder) {

        this.allOption = 0;
        this.chartDatasets = [];
        this.chartLabels = [];
        this.updateChartOptions();

        this.colorBlindEnabled = false;

        this.chartType = 'pie';
        this.chartLegend = true;


        this.hasReport = false;


        this.isComputingData = false;
        this.isLoadingData = false;
        this.isLoadingDataError = false;

        this.filtersForm = this.fb.group({
            fromDate: new FormControl({ value: null, disabled: true }),
            membersSelect: new FormControl(),
            projectSelect: new FormControl(),
            toDate: new FormControl({ value: null, disabled: true })
        });

    }

    /**
     * Update the options linked to the current chart
     */
    private updateChartOptions() {
        const options = {
            scaleShowVerticalLines: true,
            responsive: true
        };

        if (this.chartType === 'horizontalBar') {
            options['scales'] = {
                xAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            };
        }
        this.chartOptions = options;
    }

    /**
     * Update the color of the chart according to the chart type
     * and the Accessibility option and set the chartColors
     */
    private updateChartColor() {

        const nbColor = this.chartLabels.length > 0 ? this.chartLabels.length : 1;
        const listColors = ColorUtils.getRandomColorsHex(nbColor);

        const pattern = generate(listColors);

        const colorsUsed: Array<any> = this.colorBlindEnabled ? pattern : listColors;

        let res: any;

        if (this.chartType === 'pie') {
            res = [{ 'backgroundColor': colorsUsed }];
        } else {
            res = colorsUsed.map(c => ({ 'backgroundColor': c }));
        }

        this.chartColors = res;
    }

    /**
     * Called when the user (un)checks the Accessibility option
     * Add patterns to the charts color to better help people
     * with optical disorders
     */
    public toggleAccessibility() {
        this.colorBlindEnabled = !this.colorBlindEnabled;
        this.updateChartColor();
    }

    /**
     * Switch chart type and update chart
     */
    public updateChartType() {

        console.log('Radio Changed', this.chartDatasets);
        this.chartType = this.chartType === 'pie' ? 'horizontalBar' : 'pie';
        this.computeReport();
        this.updateChartOptions();
    }

    /**
     * Called when user change from date
     */
    public changeFromDate() {
        this.fromDate = this.filtersForm.controls.fromDate.value;
        this.computeReport();
    }

    /**
     * Called when user change to date
     */
    public changeToDate() {
        this.toDate = this.filtersForm.controls.toDate.value;
        this.computeReport();
    }

    /**
     * Display the names of the selected members when the filter is active
     * 
     * @return The name of the members
     */
    public getMTrigerred(): string[] {
        if (CheckUtils.isEmpty(this.filtersForm.controls.membersSelect.value)) {
            return null;
        } else {
            return this.project.members.filter(m => this.filtersForm.controls.membersSelect.value.includes(m.id))
                .map(u => u.name);
        }
    }

    /**
     * Indicates if filter is visible
     *
     * @return Boolean
     */
    public showFilter(): boolean {
        return !CheckUtils.isEmpty(this.project);
    }

    /**
     * Indicates if chart is visible
     *
     * @return Boolean
     */
    public reportComputed(): boolean {
        return !CheckUtils.isEmpty(this.chartDatasets);
    }

    /**
     * Called when user click all for members
     */
    public toggleMAll() {
        if (this.allMembers.selected) {
            this.filtersForm.controls.membersSelect.patchValue(
                [...this.project.members.map(item => item.id), 0]
            );
        } else {
            this.filtersForm.controls.membersSelect.patchValue([]);
        }

        this.computeReport();
    }

    /**
     * Called when user click in one member
     */
    public toggleMOne() {
        if (this.allMembers.selected) {
            this.allMembers.deselect();
        }
        
        if (this.filtersForm.controls.membersSelect.value.length === this.project.members.length) {
            this.allMembers.select();
        }

        this.computeReport();
    }

    public changeProject() {
        if (!CheckUtils.isEmpty(this.filtersForm.controls.projectSelect.value)) {
            this.loadData(this.filtersForm.controls.projectSelect.value);
        }
    }

    /**
     * Load all issues for all projects
     *
     * @param projectId Id of the project
     */
    public loadData(projectId: number) {
        this.isLoadingData = true;
        this.isLoadingDataError = false;
        this.project = this.rawData.getProject(projectId);

        if (CheckUtils.isNullOrUndefined(this.project)) {
            this.isLoadingDataError = true;
            this.isLoadingData = false;
        } else {
            console.log(this.project);
            Observable.forkJoin(
                this.projectsService.getAllProjectMembersPages(this.project),
                this.projectsService.getAllProjectLabelsPages(this.project)
            ).subscribe(
                () => {
                },
                (err) => {
                    console.log(err);
                    this.isLoadingData = false;
                    this.isLoadingDataError = true;
                },
                () => {
                    this.isLoadingData = false;
                    this.listIssues = this.rawData.issues.filter(issue => issue.project_id === this.project.id && issue.time_stats.total_time_spent > 0);
                    this.computeReport();
                }
            );
        }

    }

    /**
     * Take data computed from the issues and set the charts
     * parameters accordingly.
     * 
     * @param data An object with the time spent and estimated
     */
    private constructChart(data: Array<any>) {

        // Reset Values of chart 
        this.chartLabels.length = 0;

        let newChartData: Array<any>;

        if (this.chartType === 'pie') {

            newChartData = [{ 'data': [] }];

            data.forEach(element => {
                this.chartLabels.push(element.title);
                newChartData[0].data.push(element.total_time / 3600);
            });

        } else if (this.chartType === 'horizontalBar') {
            newChartData = [
                {
                    'data': [],
                    'label': 'Time spent'
                },
                {
                    'data': [],
                    'label': 'Estimated time'
                }];

            data.forEach(element => {
                this.chartLabels.push(element.title);
                newChartData[0].data.push(element.total_time / 3600);
                newChartData[1].data.push(element.estimated_time / 3600);
            });

        }
        this.isComputingData = false;
        this.hasReport = true;

        this.chartDatasets = newChartData;

        this.updateChartColor();

        console.log('newCChartDATA', this.chartDatasets);


    }

    /**
     * Get all the timelogs corresponding to the selected filters
     * 
     * @return An array of issues, each containing an array of timelogs
     */
    private getTimelogs(): Array<Array<TimeLog>> {

        const timelogs: Array<Array<TimeLog>> = [];

        const sMembers = this.filtersForm.controls.membersSelect.value;
        const fromDate = this.filtersForm.controls.fromDate.value;
        const toDate = this.filtersForm.controls.toDate.value;

        let users = this.project.members;

        console.log(sMembers);
        if (sMembers !== null) {
            users = this.project.members.filter(member => sMembers.includes(member.id));
        }


        // Get the Timelogs for each issue in the project
        this.listIssues.forEach(issue => {
            const timelog = this.rawData.getTimeLogsForProjectIssueByUsers(this.project.id, users, issue, fromDate, toDate);
            if (timelog.length > 0) {
                timelogs.push(timelog);
            }
        });

        if (timelogs.length === 0) {
            this.hasReport = false;
        }

        return timelogs;
    }

    /**
     * Sum all the times logged by issue and sort the results in descending order
     * @param timelogs The timelogs array of the issues
     */
    private computeDataFromTimelogs(timelogs: Array<Array<TimeLog>>): Array<any> {

        const data = timelogs.map(issue => ({
            'title': issue[0].noteable_title,
            'total_time': issue.map(it => it.time_logged).reduce((a, b) => a + b, 0),
            'estimated_time': this.listIssues.find(li => li.id === issue[0].noteable_id).time_stats.time_estimate
        }));

        data.sort((a, b) => {
            if (a.total_time > b.total_time) {
                return -1;
            }
            if (a.total_time < b.total_time) {
                return 1;
            }
        });

        return data;

    }

    /**
     * Compute data to report
     */
    private computeReport() {
        this.isComputingData = true;

        const timelogs: Array<Array<TimeLog>> = this.getTimelogs();

        const data = this.computeDataFromTimelogs(timelogs);
        console.log('data', data);

        this.constructChart(data);

        this.isComputingData = false;

    }
}
