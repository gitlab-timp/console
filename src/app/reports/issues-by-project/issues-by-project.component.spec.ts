/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IssuesByProjectComponent} from './issues-by-project.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ChartsModule} from 'ng2-charts';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from '../../material.module';

describe('IssuesByProjectComponent', () => {
    let component: IssuesByProjectComponent;
    let fixture: ComponentFixture<IssuesByProjectComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                IssuesByProjectComponent
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                ChartsModule,
                CommonModule,
                FormsModule,
                HttpClientModule,
                MaterialModule,
                ReactiveFormsModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IssuesByProjectComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
