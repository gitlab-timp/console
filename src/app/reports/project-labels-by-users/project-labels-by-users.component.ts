/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {RawData} from '../../model/raw-data';
import {MatOption} from '@angular/material';
import {Project} from '../../entity/project';
import {ProjectsService} from '../../service/git-lab/projects/projects.service';
import {CheckUtils} from '../../utils/check.utils';
import {Observable} from 'rxjs';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {TimeLog} from '../../model/time-log';
import {LabelTime} from '../../model/label-time';
import {Label} from '../../entity/label';
import {User} from '../../entity/user';
import {ChartData} from '../../entity/chart-data';
import {TimeUtils} from '../../utils/time.utils';
import {generate} from 'patternomaly';
import {ColorUtils} from '../../utils/color.utils';
import {ChartColor} from '../../entity/chart-color';
import {BaseChartDirective} from 'ng2-charts';

@Component({
    selector: 'app-project-labels-by-users',
    templateUrl: './project-labels-by-users.component.html',
    styleUrls: ['./project-labels-by-users.component.css']
})

export class ProjectLabelsByUsersComponent implements OnInit {

    @ViewChild('baseChart') chart: BaseChartDirective;
    @ViewChild('allMembers') private allMembers: MatOption;
    @ViewChild('allLabels') private allLabels: MatOption;

    @Input() rawData: RawData;

    public allOption: number;
    public chartColors: Array<ChartColor>;
    public chartData: Array<ChartData>;
    public chartLabels: Array<String>;
    public chartLegend: boolean;
    public chartOptions: any;
    public chartType: string;
    public filtersForm: FormGroup;
    public fromDate: Date;
    public hasReport: boolean;
    public isComputingData: boolean;
    public isLoadingData: boolean;
    public isLoadingDataError: boolean;
    public project: Project;
    public selectedLabels: string;
    public toDate: Date;

    constructor(private projectsService: ProjectsService, private fb: FormBuilder) {
        this.init();
    }

    ngOnInit() {
        this.init();

        // Sort projects by name space
        this.rawData.projects = this.rawData.projects.sort((it, ot) => {
            if (it.path_with_namespace > ot.path_with_namespace) {
                return 1;
            }

            if (it.path_with_namespace < ot.path_with_namespace) {
                return -1;
            }

            return 0;
        });
    }

    /**
     * Initiate all values
     */
    private init() {
        this.allOption = 0;
        this.chartData = [];
        this.chartLabels = [];
        this.chartOptions = {
            barThickness: 25,
            responsive: true,
            scaleShowVerticalLines: true
        };

        this.chartType = 'horizontalBar';
        this.chartLegend = true;

        this.filtersForm = this.fb.group({
            fromDate: new FormControl({value: null, disabled: true}),
            labelsSelect: new FormControl(),
            projectSelect: new FormControl(),
            toDate: new FormControl({value: null, disabled: true})
        });

        this.fromDate = null;
        this.hasReport = false;
        this.isComputingData = false;
        this.isLoadingData = false;
        this.isLoadingDataError = false;
        this.project = null;
        this.selectedLabels = null;
        this.toDate = null;
    }

    /**
     * Load all issues for all projects
     *
     * @param projectId Id of the project
     */
    public loadData(projectId: number) {
        this.isLoadingData = true;
        this.isLoadingDataError = false;
        this.project = this.rawData.getProject(projectId);

        if (CheckUtils.isNullOrUndefined(this.project)) {
            this.isLoadingDataError = true;
            this.isLoadingData = false;
        } else {
            Observable.forkJoin(
                this.projectsService.getAllProjectMembersPages(this.project),
                this.projectsService.getAllProjectLabelsPages(this.project)
            ).subscribe(
                () => {
                },
                (err) => {
                    console.log(err);
                    this.isLoadingData = false;
                    this.isLoadingDataError = true;
                },
                () => {
                    this.isLoadingData = false;


                    this.project.members = this.project.members.sort((it, ot) => {
                        if (it.name > ot.name) {
                            return 1;
                        }

                        if (it.name < ot.name) {
                            return -1;
                        }

                        return 0;
                    });

                    this.project.labels = this.project.labels.sort((it, ot) => {
                        if (it.name > ot.name) {
                            return 1;
                        }

                        if (it.name < ot.name) {
                            return -1;
                        }

                        return 0;
                    });

                    this.selectAllLabels();
                    this.computeReport();
                }
            );
        }
    }

    /**
     * Indicates if filter is visible
     *
     * @return Boolean
     */
    public showFilter(): boolean {
        return !CheckUtils.isEmpty(this.project);
    }

    /**
     * Comnpute string to show after members change
     *
     * @return String to show after select members
     */
    public updateSelectedLabels() {
        if (CheckUtils.isEmpty(this.filtersForm.controls.labelsSelect.value)) {
            this.selectedLabels = null;
        } else {
            const l = this.filtersForm.controls.labelsSelect.value.length;
            const v = this.filtersForm.controls.labelsSelect.value[0];
            const m = this.project.labels.filter(item => item.id === v);
            this.selectedLabels = (CheckUtils.isEmpty(m)) ? null : `${m[0].name} (+${l - 1})`;
        }
    }

    /**
     * Select all labels
     */
    public selectAllLabels() {
        this.filtersForm.controls.labelsSelect.patchValue([...this.project.labels.map(item => item.id), 0]);
        this.updateSelectedLabels();
    }

    /**
     * Called when user click all for labels
     */
    public toggleLAll() {
        if (this.allLabels.selected) {
            this.selectAllLabels();
        } else {
            this.filtersForm.controls.labelsSelect.patchValue([]);
        }

        this.updateSelectedLabels();
        this.computeReport();
    }

    /**
     * Called when user click in one label
     */
    public toggleLOne() {
        if (this.allLabels.selected) {
            this.allLabels.deselect();
        }

        if (this.filtersForm.controls.labelsSelect.value.length === this.project.labels.length) {
            this.allLabels.select();
        }

        this.updateSelectedLabels();
        this.computeReport();
    }

    /**
     * Called when user change from date
     */
    public changeFromDate() {
        this.fromDate = this.filtersForm.controls.fromDate.value;
        this.computeReport();
    }

    /**
     * Called when user change to date
     */
    public changeToDate() {
        this.toDate = this.filtersForm.controls.toDate.value;
        this.computeReport();
    }

    /**
     * Called when user changes project
     */
    public changeProject() {
        if (!CheckUtils.isEmpty(this.filtersForm.controls.projectSelect.value)) {
            this.loadData(this.filtersForm.controls.projectSelect.value);
        }
    }

    /**
     * Create an array of label times
     *
     * @param members Array of selected members
     * @param labels Array of selected labels
     * @param timeLogs Array of time logged
     *
     * @return Array of label times completed
     */
    private computeLabelTimes(members: Array<User>, labels: Array<Label>, timeLogs: Array<TimeLog>): Array<LabelTime> {
        let labelTimes: Array<LabelTime> = [];

        console.log(timeLogs);

        if (CheckUtils.isEmpty(timeLogs)) {
            return labelTimes;
        }

        // initialize label time array
        for (const l of labels) {
            labelTimes.push(new LabelTime(l.id, l.name, members));
        }

        for (const tl of timeLogs) {
            for (const l of tl.labels) {
                const lTimes = labelTimes.filter(it => it.label_name === l);

                if (!CheckUtils.isEmpty(lTimes)) {
                    lTimes[0].addTime(tl);
                }
            }
        }

        labelTimes = labelTimes.sort((it, ot) => {
            if (it.getTotalTimeLogged() < ot.getTotalTimeLogged()) {
                return 1;
            }

            if (it.getTotalTimeLogged() > ot.getTotalTimeLogged()) {
                return -1;
            }

            return 0;
        });

        return labelTimes;
    }


    /**
     * Construct chart data to display
     *
     * @param labelTimes Array of label times
     */
    private constructChart(labelTimes: Array<LabelTime>) {

        console.log(labelTimes);

        if (CheckUtils.isEmpty(labelTimes)) {
            this.hasReport = false;
        } else {
            // Reset Values of chart
            this.chartLabels = [];
            this.chartData = [];

            for (const lt of labelTimes) {
                this.chartLabels.push(lt.label_name);

                for (const m of lt.members_times) {
                    // Prevent mistakes if multiple users have the same name
                    const tmpLabel = `${m.member_id.toString()}_${m.member_name}`;
                    const data = this.chartData.filter((it: ChartData) => it.label === tmpLabel);
                    const value = TimeUtils.convertSecondsToHours(m.getTotalTimeLogged());

                    if (CheckUtils.isEmpty(data)) {
                        this.chartData.push({
                            data: [value],
                            label: tmpLabel
                        });
                    } else {
                        data[0].data.push(value);
                    }
                }
            }

            // Replace tmp label by good label
            this.chartData = this.chartData.map((it: ChartData) => {
                return {data: it.data, label: it.label.split('_')[1]};
            });

            console.log(this.chartData);

            this.chartColors = generate(ColorUtils.getRandomColorsHex(this.chartData.length)).map(it => {
                return {
                    backgroundColor: it,
                    borderColor: it,
                    pointBackgroundColor: it,
                    pointBorderColor: it,
                    pointHoverBackgroundColor: it,
                    pointHoverBorderColor: it
                };
            });

            this.reloadChart();
            this.hasReport = true;
        }

        this.isComputingData = false;
    }

    /**
     * Force chart redraw
     */
    private reloadChart() {
        if (this.chart !== undefined) {
            this.chart.chart.destroy();
            this.chart.chart = 0;

            this.chart.datasets = this.chartData;
            this.chart.labels = this.chartLabels;
            this.chart.ngOnInit();
        }
    }

    /**
     * Compute data to report
     */
    private computeReport() {
        this.isComputingData = true;

        const projectId = this.filtersForm.controls.projectSelect.value;
        const sLabels = this.filtersForm.controls.labelsSelect.value;

        if (CheckUtils.isEmpty(projectId) && CheckUtils.isEmpty(this.project.members) && CheckUtils.isEmpty(sLabels)) {
            this.isComputingData = false;
            return false;
        }

        const labels = this.project.labels.filter(item => sLabels.includes(item.id));
        const timeLogs = this.rawData.getTimeLogsProjectLabelsByUser(
            projectId, this.project.members, labels, this.filtersForm.controls.fromDate.value, this.filtersForm.controls.toDate.value
        );

        this.constructChart(this.computeLabelTimes(this.project.members, labels, timeLogs));
    }

}
