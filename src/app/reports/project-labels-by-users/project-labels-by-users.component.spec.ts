/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProjectLabelsByUsersComponent} from './project-labels-by-users.component';
import {RawData} from '../../model/raw-data';
import {ChartsModule} from 'ng2-charts';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from '../../material.module';

describe('Component: ProjectLabelsByUsers', () => {
    let component: ProjectLabelsByUsersComponent;
    let fixture: ComponentFixture<ProjectLabelsByUsersComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ProjectLabelsByUsersComponent
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                ChartsModule,
                CommonModule,
                FormsModule,
                HttpClientModule,
                MaterialModule,
                ReactiveFormsModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProjectLabelsByUsersComponent);
        component = fixture.componentInstance;
        component.rawData = new RawData();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
