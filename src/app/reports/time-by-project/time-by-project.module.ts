/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {NgModule} from '@angular/core';
import {TimeByProjectComponent} from './time-by-project.component';
import {ChartsModule} from 'ng2-charts';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../material.module';

@NgModule({
    declarations: [
        TimeByProjectComponent
    ],
    exports: [
        TimeByProjectComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ChartsModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        ReactiveFormsModule
    ]
})

export class TimeByProjectModule {
}
