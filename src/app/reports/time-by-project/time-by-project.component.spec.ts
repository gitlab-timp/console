/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TimeByProjectComponent} from './time-by-project.component';
import {ChartsModule} from 'ng2-charts';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from '../../material.module';
import {RawData} from 'src/app/model/raw-data';

describe('TimeByProjectComponent', () => {
    let component: TimeByProjectComponent;
    let fixture: ComponentFixture<TimeByProjectComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TimeByProjectComponent],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                ChartsModule,
                CommonModule,
                FormsModule,
                HttpClientModule,
                MaterialModule,
                ReactiveFormsModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TimeByProjectComponent);
        component = fixture.componentInstance;
        component.rawData = new RawData();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
