/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {RawData} from '../../model/raw-data';
import {MatOption} from '@angular/material';
import {Project} from '../../entity/project';
import {ProjectsService} from '../../service/git-lab/projects/projects.service';
import {CheckUtils} from '../../utils/check.utils';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {TimeLog} from '../../model/time-log';
import {User} from '../../entity/user';
import {TimeUtils} from '../../utils/time.utils';
import {generate} from 'patternomaly';
import {ColorUtils} from '../../utils/color.utils';
import {MemberTime} from '../../model/member-time';

@Component({
    selector: 'app-time-by-project',
    templateUrl: './time-by-project.component.html',
    styleUrls: ['./time-by-project.component.css']
})
export class TimeByProjectComponent implements OnInit {

    @ViewChild('allMembers') private allMembers: MatOption;

    @Input() rawData: RawData;

    public allOption: number;
    public chartColors: Array<any>;
    public chartData: Array<number>;
    public chartLabels: Array<String>;
    public chartLegend: boolean;
    public chartOptions: any;
    public chartType: string;
    public filtersForm: FormGroup;
    public fromDate: Date;
    public hasReport: boolean;
    public isComputingData: boolean;
    public isLoadingData: boolean;
    public isLoadingDataError: boolean;
    public project: Project;
    public toDate: Date;

    constructor(private projectsService: ProjectsService, private fb: FormBuilder) {
        this.init();
    }

    ngOnInit() {
        this.init();

        // Sort projects by name space
        this.rawData.projects = this.rawData.projects.sort((it, ot) => {
            if (it.path_with_namespace > ot.path_with_namespace) {
                return 1;
            }

            if (it.path_with_namespace < ot.path_with_namespace) {
                return -1;
            }

            return 0;
        });
    }

    /**
     * Initiate all values
     */
    private init() {
        this.allOption = 0;
        this.chartData = [];
        this.chartLabels = [];
        this.chartOptions = {
            responsive: true,
            scaleShowVerticalLines: true
        };

        this.chartType = 'polarArea';
        this.chartLegend = true;

        this.filtersForm = this.fb.group({
            fromDate: new FormControl({value: null, disabled: true}),
            projectSelect: new FormControl(),
            toDate: new FormControl({value: null, disabled: true})
        });

        this.fromDate = null;
        this.hasReport = false;
        this.isComputingData = false;
        this.isLoadingData = false;
        this.isLoadingDataError = false;
        this.project = null;
        this.toDate = null;
    }

    /**
     * Load all issues for all projects
     *
     * @param projectId Id of the project
     */
    public loadData(projectId: number) {
        this.isLoadingData = true;
        this.isLoadingDataError = false;
        this.project = this.rawData.getProject(projectId);

        if (CheckUtils.isNullOrUndefined(this.project)) {
            this.isLoadingDataError = true;
            this.isLoadingData = false;
        } else {
            this.projectsService.getAllProjectMembersPages(this.project).subscribe(
                () => {
                },
                (err) => {
                    console.log(err);
                    this.isLoadingData = false;
                    this.isLoadingDataError = true;
                },
                () => {
                    this.isLoadingData = false;

                    this.project.members = this.project.members.sort((it, ot) => {
                        if (it.name > ot.name) {
                            return 1;
                        }

                        if (it.name < ot.name) {
                            return -1;
                        }

                        return 0;
                    });

                    this.computeReport();
                }
            );
        }
    }

    /**
     * Indicates if filter is visible
     *
     * @return Boolean
     */
    public showFilter(): boolean {
        return !CheckUtils.isEmpty(this.project);
    }

    /**
     * Called when user change from date
     */
    public changeFromDate() {
        this.fromDate = this.filtersForm.controls.fromDate.value;
        this.computeReport();
    }

    /**
     * Called when user change to date
     */
    public changeToDate() {
        this.toDate = this.filtersForm.controls.toDate.value;
        this.computeReport();
    }

    /**
     * Called when user changes project
     */
    public changeProject() {
        if (!CheckUtils.isEmpty(this.filtersForm.controls.projectSelect.value)) {
            this.loadData(this.filtersForm.controls.projectSelect.value);
        }
    }

    /**
     * Construct chart data to display
     *
     * @param timeLogs Array of time logged
     */
    private constructChart(timeLogs: Array<TimeLog>) {
        console.log(timeLogs);

        if (CheckUtils.isEmpty(timeLogs)) {
            this.hasReport = false;
        } else {
            // Reset Values of chart
            this.chartLabels.length = 0;
            const newChartData: Array<number> = [];

            this.project.members.forEach((m: User) => {
                const mt = new MemberTime(m.id, m.name);
                mt.time_logs = timeLogs.filter((it: TimeLog) => it.author_id === m.id);
                this.chartLabels.push(m.name);
                newChartData.push(TimeUtils.convertSecondsToHours(mt.getTotalTimeLogged()));

            });

            this.chartData = newChartData;

            this.chartColors = [
                {
                    backgroundColor: generate(ColorUtils.getRandomColorsHex(this.chartData.length))
                }
            ];

            console.log(this.chartLabels, this.chartData, this.chartData.length, this.chartColors);

            this.hasReport = true;
        }

        this.isComputingData = false;
    }

    /**
     * Compute data to report
     */
    private computeReport() {
        if (CheckUtils.isEmpty(this.filtersForm.controls.projectSelect.value) && CheckUtils.isEmpty(this.project.members)) {
            this.isComputingData = false;
        } else {
            this.isComputingData = true;

            this.constructChart(this.rawData.getTimeLogsForProject(
                this.filtersForm.controls.projectSelect.value,
                this.project.members,
                this.filtersForm.controls.fromDate.value,
                this.filtersForm.controls.toDate.value
            ));
        }
    }

}
