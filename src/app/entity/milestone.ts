/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


export interface Milestone {
    id: number;
    iid: number;
    project_id: number;
    title: string;
    description: string;
    state: string;
    created_at: string;
    updated_at: string;
    due_date: string;
    start_date: string;
    web_url: string;
}
