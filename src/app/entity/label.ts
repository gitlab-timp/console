/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


export interface Label {
    id: number;
    name: string;
    color: string;
    description: string;
    open_issues_count: number;
    closed_issues_count: number;
    open_merge_requests_count: number;
    subscribed: boolean;
    priority: number;
}
