/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Milestone} from './milestone';
import {User} from './user';
import {TimeStat} from './time-stat';
import {Project} from './project';
import {Label} from './label';

export interface Issue {
    id: number;
    iid: number;
    project_id: number;
    title: string;
    description: string;
    state: string;
    created_at: string;
    updated_at: string;
    closed_at: string;
    closed_by: User;
    labels: Array<string>;
    milestone: Milestone;
    assignees: Array<User>;
    author: User;
    assignee: User;
    user_notes_count: number;
    upvotes: number;
    downvotes: number;
    due_date: string;
    confidential: boolean;
    discussion_locked: string;
    web_url: string;
    time_stats: TimeStat;
    weight: number;

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                      EXTENSION
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    project: Project;
    labels_full: Array<Label>;
}
