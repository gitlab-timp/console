/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {User} from './user';

export interface Note {
    id: number;
    type: string;
    body: string;
    attachment: string;
    author: User;
    created_at: string;
    updated_at: string;
    system: boolean;
    noteable_id: number;
    noteable_type: string;
    resolvable: boolean;
    noteable_iid: number;
}
