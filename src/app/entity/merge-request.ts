/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Milestone} from './milestone';
import {User} from './user';
import {TimeStat} from './time-stat';
import {Project} from './project';
import {Label} from './label';

export interface MergeRequest {
    id: number;
    iid: number;
    project_id: number;
    title: string;
    description: string;
    state: string;
    created_at: string;
    updated_at: string;
    target_branch: string;
    source_branch: string;
    upvotes: number;
    downvotes: number;
    author: User;
    assignee: User;
    source_project_id: number;
    target_project_id: number;
    labels: Array<string>;
    work_in_progress: boolean;
    milestone: Milestone;
    merge_when_pipeline_succeeds: boolean;
    merge_status: string;
    sha: string;
    merge_commit_sha: string;
    user_notes_count: number;
    discussion_locked: string;
    should_remove_source_branch: boolean;
    force_remove_source_branch: boolean;
    allow_collaboration: boolean;
    allow_maintainer_to_push: boolean;
    web_url: string;
    time_stats: TimeStat;
    squash: boolean;
    approvals_before_merge: string;
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                      EXTENSION
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    project: Project;
    labels_full: Array<Label>;
}


