/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, OnInit} from '@angular/core';

declare var $: any;

export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    i18nId: string;
}

export const ROUTES: RouteInfo[] = [
    {path: 'dashboard', title: 'Dashboard', icon: 'ti-panel', class: '', i18nId: '@@dashboard'},
    {path: 'my-work', title: 'My work', icon: 'ti-timer', class: '', i18nId: '@@my-work'},
    {path: 'reports', title: 'Reports', icon: 'ti-bar-chart', class: '', i18nId: '@@report'}
];

@Component({
    moduleId: module.id,
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html'
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    /**
     * Check if screen size is small
     */
    isMobileMenu() {
        return !($(window).width() > 991);
    }
}
