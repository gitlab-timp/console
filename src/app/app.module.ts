/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {AppRoutes} from './app.routing';
import {SidebarModule} from './sidebar/sidebar.module';
import {FooterModule} from './shared/footer/footer.module';
import {NavbarModule} from './shared/navbar/navbar.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {APP_BASE_HREF, CommonModule, registerLocaleData} from '@angular/common';
import {ChartsModule} from 'ng2-charts';
import localeFr from '@angular/common/locales/fr';
import {LoginComponent} from './login/login.component';
import {AccessTokenLoginFormComponent} from './login/access-token-login-form/access-token-login-form.component';
import {OauthLoginFormComponent} from './login/oauth-login-form/oauth-login-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthHttpInterceptor} from './interceptor/auth-http.interceptor';
import {IssuesListModule} from './issues/issues-list/issues-list.module';
import {MergeRequestsListModule} from './merge-requests/merge-requests-list/merge-requests-list.module';
import {MyWorkComponent} from './my-work/my-work.component';
import {LocaleEnum} from './enums/locale.enum';
import {LogEntryModule} from './my-work/log-entry/log-entry.module';
import {LogDayModule} from './my-work/log-day/log-day.module';
import {ReportsComponent} from './reports/reports.component';
import {ProjectLabelsByUsersModule} from './reports/project-labels-by-users/project-labels-by-users.module';
import {IssuesByProjectModule} from './reports/issues-by-project/issues-by-project.module';
import {MaterialModule} from './material.module';
import {TimeByProjectModule} from './reports/time-by-project/time-by-project.module';

registerLocaleData(localeFr, LocaleEnum.FR);

@NgModule({
    declarations: [
        AccessTokenLoginFormComponent,
        AppComponent,
        DashboardComponent,
        LoginComponent,
        MyWorkComponent,
        OauthLoginFormComponent,
        ReportsComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        ChartsModule,
        CommonModule,
        FooterModule,
        FormsModule,
        HttpClientModule,
        IssuesListModule,
        LogDayModule,
        LogEntryModule,
        MaterialModule,
        MergeRequestsListModule,
        NavbarModule,
        ProjectLabelsByUsersModule,
        TimeByProjectModule,
        IssuesByProjectModule,
        ReactiveFormsModule,
        RouterModule.forRoot(AppRoutes),
        SidebarModule
    ],
    providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
        {provide: LOCALE_ID, useValue: LocaleEnum.FR},
        {provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true}
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule {
}
