/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IssuesListComponent} from './issues-list.component';
import {HttpClientModule} from '@angular/common/http';
import {DisplayEnum} from '../../enums/display.enum';
import {IssueStateEnum} from '../../service/git-lab/issues/issue-state.enum';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../material.module';

describe('Component: IssuesList', () => {
    let component: IssuesListComponent;
    let fixture: ComponentFixture<IssuesListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                IssuesListComponent
            ],
            imports: [
                CommonModule,
                HttpClientModule,
                MaterialModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IssuesListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`getDetailDisplayMode(null) should return ${DisplayEnum.COLLAPSED}`, () => {
        const displayMode = component.getDetailDisplayMode(null);
        expect(displayMode).toBe(DisplayEnum.COLLAPSED);
    });

    it(`getDetailDisplayMode(dummyData) should return ${DisplayEnum.COLLAPSED}`, () => {
        const dummyData = {
            id: 1,
            iid: 1,
            project_id: 1,
            title: 'Pizza',
            description: '',
            state: IssueStateEnum.OPENED,
            created_at: '',
            updated_at: '',
            closed_at: '',
            closed_by: null,
            labels: [],
            milestone: null,
            assignees: [],
            author: null,
            assignee: null,
            user_notes_count: 0,
            upvotes: 0,
            downvotes: 0,
            due_date: '',
            confidential: false,
            discussion_locked: '',
            web_url: '',
            time_stats: null,
            weight: 0,
            project: null,
            labels_full: []
        };

        component.dataExpanded = {
            id: 1,
            iid: 1,
            project_id: 1,
            title: 'Pizza',
            description: '',
            state: IssueStateEnum.OPENED,
            created_at: '',
            updated_at: '',
            closed_at: '',
            closed_by: null,
            labels: [],
            milestone: null,
            assignees: [],
            author: null,
            assignee: null,
            user_notes_count: 0,
            upvotes: 0,
            downvotes: 0,
            due_date: '',
            confidential: false,
            discussion_locked: '',
            web_url: '',
            time_stats: null,
            weight: 0,
            project: null,
            labels_full: []
        };

        component.isExpandLoaded = true;

        const expandMode = component.getDetailDisplayMode(dummyData);
        expect(expandMode).toBe(DisplayEnum.EXPANDED);
    });
});
