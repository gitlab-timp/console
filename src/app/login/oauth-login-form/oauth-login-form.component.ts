/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';

@Component({
    selector: 'app-oauth-login-form',
    templateUrl: './oauth-login-form.component.html',
    styleUrls: ['./oauth-login-form.component.css']
})
export class OauthLoginFormComponent implements OnInit {
    loginForm = this.fb.group({
        clientId: [''],
        clientSecretId: ['']
    });

    constructor(private fb: FormBuilder) {
    }

    ngOnInit() {
    }

}
