/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthProviderService} from '../../service/auth-provider.service';
import {AccessTokenAuthenticator} from '../../service/git-lab/access-token-authenticator';
import {Router} from '@angular/router';

@Component({
    selector: 'app-access-token-login-form',
    templateUrl: './access-token-login-form.component.html',
    styleUrls: ['./access-token-login-form.component.css']
})
export class AccessTokenLoginFormComponent implements OnInit {
    loginForm = this.fb.group({
        accessToken: ['', Validators.required]
    });

    error = false;

    constructor(
        private fb: FormBuilder,
        private authService: AuthProviderService,
        private authenticator: AccessTokenAuthenticator,
        private router: Router) {
    }

    ngOnInit() {
    }

    /**
     * Submit form handler
     */
    onSubmit() {
        this.error = false;
        if (!this.loginForm.valid) {
            return;
        }
        const token = this.loginForm.controls['accessToken'].value;

        this.authenticator.authenticate({accessToken: token}).then(
            value => {
                if (value) {
                    this.authService.authenticator = this.authenticator;
                    this.router.navigate(['/dashboard']);
                } else {
                    this.error = true;
                }
            },
            reason => {
                console.log(reason);
                this.error = true;
            }
        );
    }

}
