/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {AppRoutes} from './app.routing';
import {DashboardComponent} from './dashboard/dashboard.component';
import {APP_BASE_HREF} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {OauthLoginFormComponent} from './login/oauth-login-form/oauth-login-form.component';
import {AccessTokenLoginFormComponent} from './login/access-token-login-form/access-token-login-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {MyWorkComponent} from './my-work/my-work.component';
import {LogEntryModule} from './my-work/log-entry/log-entry.module';
import {BrowserModule} from '@angular/platform-browser';
import {ChartsModule} from 'ng2-charts';
import {FooterModule} from './shared/footer/footer.module';
import {IssuesListModule} from './issues/issues-list/issues-list.module';
import {LogDayModule} from './my-work/log-day/log-day.module';
import {MergeRequestsListModule} from './merge-requests/merge-requests-list/merge-requests-list.module';
import {NavbarModule} from './shared/navbar/navbar.module';
import {SidebarModule} from './sidebar/sidebar.module';
import {ReportsComponent} from './reports/reports.component';
import {ProjectLabelsByUsersModule} from './reports/project-labels-by-users/project-labels-by-users.module';
import {MaterialModule} from './material.module';
import {TimeByProjectModule} from './reports/time-by-project/time-by-project.module';
import {IssuesByProjectModule} from './reports/issues-by-project/issues-by-project.module';

describe('Component: App', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AccessTokenLoginFormComponent,
                AppComponent,
                DashboardComponent,
                LoginComponent,
                MyWorkComponent,
                OauthLoginFormComponent,
                ReportsComponent
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                HttpClientModule,
                ChartsModule,
                FooterModule,
                FormsModule,
                IssuesListModule,
                LogDayModule,
                LogEntryModule,
                MaterialModule,
                MergeRequestsListModule,
                NavbarModule,
                ProjectLabelsByUsersModule,
                TimeByProjectModule,
                IssuesByProjectModule,
                ReactiveFormsModule,
                RouterModule.forRoot(AppRoutes),
                SidebarModule
            ],
            providers: [
                {provide: APP_BASE_HREF, useValue: '/'}
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the app', async(() => {
        expect(component).toBeTruthy();
    }));
});
