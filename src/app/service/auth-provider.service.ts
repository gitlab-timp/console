/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {Authenticator} from './authenticator';
import {AccessTokenAuthenticator} from './git-lab/access-token-authenticator';

@Injectable({
    providedIn: 'root'
})
export class AuthProviderService {
    private _authenticator: Authenticator = null;

    /**
     * Constructor with supported authenticators
     *
     * @param accessAuthenticator Authenticator for accesstoken
     */
    constructor(accessAuthenticator: AccessTokenAuthenticator) {
        const storedToken = Authenticator.getStoredApiToken();
        if (null !== storedToken) {
            switch (storedToken.type) {
                case AccessTokenAuthenticator.AUTH_NAME:
                    this._authenticator = accessAuthenticator;
                    break;
            }
        }
    }

    /**
     * Check if user is authenticated.
     */
    public isAuthenticated(): boolean {
        return null !== this._authenticator ? this._authenticator.isAuthenticated : false;

    }

    /**
     * Getter for authenticator
     */
    get authenticator(): Authenticator {
        return this._authenticator;
    }

    /**
     * Setter for authenticator.
     *
     * @param authenticator The authenticator
     */
    set authenticator(authenticator: Authenticator) {
        this._authenticator = authenticator;
    }
}
