/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {UsersService} from './users.service';
import {GitLabPathsEnum} from '../git-lab-paths.enum';
import {User} from '../../../entity/user';

describe('Service: UsersService', () => {
    let injector: TestBed;
    let service: UsersService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [UsersService]
        });

        injector = getTestBed();
        service = injector.get(UsersService);
        httpMock = injector.get(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should return a User', () => {
        const dummyData: User = {
            id: 1,
            name: 'test',
            username: 'test',
            state: '',
            avatar_url: '',
            web_url: ''
        };

        service.getAuthenticated().subscribe((data: User) => {
            expect(data.id).toBe(1);
            expect(data).toEqual(dummyData);
        });

        const testRequest = httpMock.expectOne(req =>
            req.method === 'GET' && req.url === `${GitLabPathsEnum.BASE_PATH}/user`);
        expect(testRequest.request.method).toBe('GET');
        testRequest.flush(dummyData);
    });

});
