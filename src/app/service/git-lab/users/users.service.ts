/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ObserveEnum} from '../../observe.enum';
import {GitLabPathsEnum} from '../git-lab-paths.enum';
import {User} from '../../../entity/user';


@Injectable({
    providedIn: 'root'
})

export class UsersService {

    constructor(protected httpClient: HttpClient) {
    }

    /**
     * Get authenticated user
     *
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress Flag to report request and response progress.
     */
    public getAuthenticated(observe?: ObserveEnum.BODY, reportProgress?: boolean): Observable<User>;
    public getAuthenticated(observe?: ObserveEnum.RESPONSE, reportProgress?: boolean): Observable<HttpResponse<User>>;
    public getAuthenticated(observe?: ObserveEnum.EVENTS, reportProgress?: boolean): Observable<HttpEvent<User>>;
    public getAuthenticated(observe: any = ObserveEnum.BODY, reportProgress: boolean = false): Observable<any> {
        const params = new HttpParams();

        return this.httpClient.get<Array<User>>(`${GitLabPathsEnum.BASE_PATH}/user`,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

}
