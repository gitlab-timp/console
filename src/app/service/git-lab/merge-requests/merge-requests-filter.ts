/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {MergeRequestStateEnum} from './merge-request-state.enum';
import {MergeRequestScopeEnum} from './merge-request-scope.enum';

export interface MergeRequestsFilter {
    state?: MergeRequestStateEnum;
    scope?: MergeRequestScopeEnum;
}
