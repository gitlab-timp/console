/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {GitLabPathsEnum} from '../git-lab-paths.enum';
import {MergeRequestsService} from './merge-requests.service';
import {MergeRequest} from '../../../entity/merge-request';
import {MergeRequestStateEnum} from './merge-request-state.enum';
import {HttpResponse} from '@angular/common/http';
import {ObserveEnum} from '../../observe.enum';

describe('Service: MergeRequestServive', () => {
    let injector: TestBed;
    let service: MergeRequestsService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [MergeRequestsService]
        });

        injector = getTestBed();
        service = injector.get(MergeRequestsService);
        httpMock = injector.get(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    describe('find', () => {
        it('should return an Observable<Array<MergeRequest>>', () => {
            const dummyData: Array<MergeRequest> = [
                {
                    id: 1,
                    iid: 1,
                    project_id: 1,
                    title: '',
                    description: '',
                    state: MergeRequestStateEnum.OPENED,
                    created_at: '',
                    updated_at: '',
                    target_branch: '',
                    source_branch: '',
                    upvotes: 1,
                    downvotes: 1,
                    author: null,
                    assignee: null,
                    source_project_id: 1,
                    target_project_id: 1,
                    labels: [],
                    work_in_progress: false,
                    milestone: null,
                    merge_when_pipeline_succeeds: false,
                    merge_status: '',
                    sha: '',
                    merge_commit_sha: '',
                    user_notes_count: 1,
                    discussion_locked: '',
                    should_remove_source_branch: false,
                    force_remove_source_branch: false,
                    allow_collaboration: false,
                    allow_maintainer_to_push: false,
                    web_url: '',
                    time_stats: null,
                    squash: false,
                    approvals_before_merge: '',
                    project: null,
                    labels_full: []
                },
                {
                    id: 2,
                    iid: 2,
                    project_id: 2,
                    title: '',
                    description: '',
                    state: MergeRequestStateEnum.OPENED,
                    created_at: '',
                    updated_at: '',
                    target_branch: '',
                    source_branch: '',
                    upvotes: 2,
                    downvotes: 2,
                    author: null,
                    assignee: null,
                    source_project_id: 2,
                    target_project_id: 2,
                    labels: [],
                    work_in_progress: false,
                    milestone: null,
                    merge_when_pipeline_succeeds: false,
                    merge_status: '',
                    sha: '',
                    merge_commit_sha: '',
                    user_notes_count: 2,
                    discussion_locked: '',
                    should_remove_source_branch: false,
                    force_remove_source_branch: false,
                    allow_collaboration: false,
                    allow_maintainer_to_push: false,
                    web_url: '',
                    time_stats: null,
                    squash: false,
                    approvals_before_merge: '',
                    project: null,
                    labels_full: []
                }
            ];

            service.find(1, null, ObserveEnum.RESPONSE).subscribe((res: HttpResponse<Array<MergeRequest>>) => {
                expect(res.body.length).toBe(2);
                expect(res.body).toEqual(dummyData);
            });

            const testRequest = httpMock.expectOne(req =>
                req.method === 'GET' && req.url === GitLabPathsEnum.MERGE_REQUESTS_PATH);
            expect(testRequest.request.method).toBe('GET');
            testRequest.flush(dummyData);
        });
    });
});
