/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {MergeRequest} from '../../../entity/merge-request';
import {ObserveEnum} from '../../observe.enum';
import {GitLabPathsEnum} from '../git-lab-paths.enum';
import {GitLabService} from '../git-lab.service';
import {MergeRequestsFilter} from './merge-requests-filter';
import {Issue} from '../../../entity/issue';
import {CheckUtils} from '../../../utils/check.utils';
import {expand} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {RawData} from '../../../model/raw-data';


@Injectable({
    providedIn: 'root'
})

export class MergeRequestsService {

    constructor(protected httpClient: HttpClient) {
    }

    /**
     * Set filter in http params
     *
     * @param filter Filter to apply to query
     * @param params Current Http Params
     */
    private static setQueryParamsFilter(params: HttpParams, filter?: MergeRequestsFilter): HttpParams {
        if (!CheckUtils.isNullOrUndefined(filter)) {
            if (!CheckUtils.isNullOrUndefined(filter.state)) {
                params = params.append('state', filter.state);
            }

            if (!CheckUtils.isNullOrUndefined(filter.scope)) {
                params = params.append('scope', filter.scope);
            }
        }

        return params;
    }

    /**
     * Finds MergeRequest
     *
     * @param page Page to return
     * @param filter Filter to apply to MergeRequest
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param pageSize Number of items returned by page
     * @param reportProgress Flag to report request and response progress.
     */
    public find(page?: number, filter?: MergeRequestsFilter, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<MergeRequest>>;
    public find(page?: number, filter?: MergeRequestsFilter, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<MergeRequest>>>;
    public find(page?: number, filter?: MergeRequestsFilter, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<MergeRequest>>>;
    public find(page?: number, filter?: MergeRequestsFilter, observe: any = ObserveEnum.RESPONSE, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        const params = MergeRequestsService.setQueryParamsFilter(GitLabService.initializeHttpParams(page, pageSize), filter);

        return this.httpClient.get<Array<MergeRequest>>(GitLabPathsEnum.MERGE_REQUESTS_PATH,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Finds MergeRequest for project
     *
     * @param projectId Project id
     * @param page Page to return
     * @param filter Filter to apply to issues
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param pageSize Number of items returned by page
     * @param reportProgress Flag to report request and response progress.
     */
    public findByProject(projectId: number, page?: number, filter?: MergeRequestsFilter, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<Issue>>;
    public findByProject(projectId: number, page?: number, filter?: MergeRequestsFilter, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<Issue>>>;
    public findByProject(projectId: number, page?: number, filter?: MergeRequestsFilter, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<Issue>>>;
    public findByProject(projectId: number, page?: number, filter?: MergeRequestsFilter, observe: any = ObserveEnum.RESPONSE, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        const params = MergeRequestsService.setQueryParamsFilter(GitLabService.initializeHttpParams(page, pageSize), filter);

        return this.httpClient.get<Array<Issue>>(`${GitLabPathsEnum.PROJECTS_PATH}/${projectId}/merge_requests`,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Load all projects merge requests recursively
     *
     * @param rawData Object to store all data loaded
     * @param projectId Id of the project
     *
     * @return Observable<Array<MergeRequest>>
     */
    public getAllMergeRequestsByProject(rawData: RawData, projectId: number): Observable<Array<MergeRequest>> {
        const getPage = (page?: number): Observable<any> => {
            return this.findByProject(projectId, page);
        };

        return getPage().pipe(expand((res: HttpResponse<Array<MergeRequest>>) => {
            const nextPage = GitLabService.getNexPage(res.headers);
            rawData.mergeRequests = rawData.mergeRequests.concat(res.body).filter((item, index, self) => {
                return self.findIndex((other) => other.id === item.id) === index;
            });
            return (GitLabService.isLastPage(nextPage)) ? EMPTY : getPage(nextPage);
        }));
    }

}
