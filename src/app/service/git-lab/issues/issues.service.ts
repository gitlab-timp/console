/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Issue} from '../../../entity/issue';
import {ObserveEnum} from '../../observe.enum';
import {GitLabPathsEnum} from '../git-lab-paths.enum';
import {GitLabService} from '../git-lab.service';
import {IssuesFilter} from './issues-filter';
import {CheckUtils} from '../../../utils/check.utils';
import {expand} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {RawData} from '../../../model/raw-data';


@Injectable({
    providedIn: 'root'
})

export class IssuesService {

    constructor(protected httpClient: HttpClient) {
    }

    /**
     * Set filter in http params
     *
     * @param filter Filter to apply to query
     * @param params Current Http Params
     */
    private static setQueryParamsFilter(params: HttpParams, filter?: IssuesFilter): HttpParams {
        if (!CheckUtils.isNullOrUndefined(filter)) {
            if (!CheckUtils.isNullOrUndefined(filter.state)) {
                params = params.append('state', filter.state);
            }

            if (!CheckUtils.isNullOrUndefined(filter.scope)) {
                params = params.append('scope', filter.scope);
            }
        }

        return params;
    }

    /**
     * Finds Issue
     *
     * @param page Page to return
     * @param filter Filter to apply to Issue
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param pageSize Number of items returned by page
     * @param reportProgress Flag to report request and response progress.
     */
    public find(page?: number, filter?: IssuesFilter, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<Issue>>;
    public find(page?: number, filter?: IssuesFilter, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<Issue>>>;
    public find(page?: number, filter?: IssuesFilter, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<Issue>>>;
    public find(page?: number, filter?: IssuesFilter, observe: any = ObserveEnum.RESPONSE, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        const params = IssuesService.setQueryParamsFilter(GitLabService.initializeHttpParams(page, pageSize), filter);

        return this.httpClient.get<Array<Issue>>(GitLabPathsEnum.ISSUES_PATH,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Finds Issue for project
     *
     * @param projectId Project id
     * @param page Page to return
     * @param filter Filter to apply to issues
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param pageSize Number of items returned by page
     * @param reportProgress Flag to report request and response progress.
     */
    public findByProject(projectId: number, page?: number, filter?: IssuesFilter, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<Issue>>;
    public findByProject(projectId: number, page?: number, filter?: IssuesFilter, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<Issue>>>;
    public findByProject(projectId: number, page?: number, filter?: IssuesFilter, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<Issue>>>;
    public findByProject(projectId: number, page?: number, filter?: IssuesFilter, observe: any = ObserveEnum.RESPONSE, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        const queryParameters = IssuesService.setQueryParamsFilter(
            GitLabService.initializeHttpParams(page, pageSize), filter
        );

        return this.httpClient.get<Array<Issue>>(`${GitLabPathsEnum.PROJECTS_PATH}/${projectId}/issues`,
            {params: queryParameters, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Load all projects issues recursively
     *
     * @param rawData Object to store all data loaded
     * @param projectId Id of the project
     *
     * @return Observable<Array<Issue>>
     */
    public getAllIssuesByProject(rawData: RawData, projectId: number): Observable<Array<Issue>> {
        const getPage = (page?: number): Observable<any> => {
            return this.findByProject(projectId, page);
        };

        return getPage().pipe(expand((res: HttpResponse<Array<Issue>>) => {
            const nextPage = GitLabService.getNexPage(res.headers);
            rawData.issues = rawData.issues.concat(res.body).filter((item, index, self) => {
                return self.findIndex((other) => other.id === item.id) === index;
            });
            return (GitLabService.isLastPage(nextPage)) ? EMPTY : getPage(nextPage);
        }));
    }
}
