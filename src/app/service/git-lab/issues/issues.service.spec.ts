/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {IssuesService} from './issues.service';
import {GitLabPathsEnum} from '../git-lab-paths.enum';
import {IssueStateEnum} from './issue-state.enum';
import {Issue} from '../../../entity/issue';
import {ObserveEnum} from '../../observe.enum';
import {HttpResponse} from '@angular/common/http';

describe('Service: IssuesService', () => {
    let injector: TestBed;
    let service: IssuesService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [IssuesService]
        });

        injector = getTestBed();
        service = injector.get(IssuesService);
        httpMock = injector.get(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    describe('find', () => {
        it('should return an Observable<Array<Issue>>', () => {
            const dummyData: Array<Issue> = [
                {
                    id: 1,
                    iid: 1,
                    project_id: 1,
                    title: 'Pizza',
                    description: '',
                    state: IssueStateEnum.OPENED,
                    created_at: '',
                    updated_at: '',
                    closed_at: '',
                    closed_by: null,
                    labels: [],
                    milestone: null,
                    assignees: [],
                    author: null,
                    assignee: null,
                    user_notes_count: 0,
                    upvotes: 0,
                    downvotes: 0,
                    due_date: '',
                    confidential: false,
                    discussion_locked: '',
                    web_url: '',
                    time_stats: null,
                    weight: 0,
                    project: null,
                    labels_full: []
                },
                {
                    id: 2,
                    iid: 2,
                    project_id: 1,
                    title: 'Tacos',
                    description: '',
                    state: IssueStateEnum.OPENED,
                    created_at: '',
                    updated_at: '',
                    closed_at: '',
                    closed_by: null,
                    labels: [],
                    milestone: null,
                    assignees: [],
                    author: null,
                    assignee: null,
                    user_notes_count: 0,
                    upvotes: 0,
                    downvotes: 0,
                    due_date: '',
                    confidential: false,
                    discussion_locked: '',
                    web_url: '',
                    time_stats: null,
                    weight: 0,
                    project: null,
                    labels_full: []
                }
            ];

            service.find(1, null, ObserveEnum.RESPONSE).subscribe((res: HttpResponse<Array<Issue>>) => {
                expect(res.body.length).toBe(2);
                expect(res.body).toEqual(dummyData);
            });

            const testRequest = httpMock.expectOne(req =>
                req.method === 'GET' && req.url === GitLabPathsEnum.ISSUES_PATH);
            expect(testRequest.request.method).toBe('GET');
            testRequest.flush(dummyData);
        });
    });
});
