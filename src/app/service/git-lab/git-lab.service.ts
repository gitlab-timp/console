/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {HttpHeaders, HttpParams} from '@angular/common/http';
import {GitlabPaginationEnum} from './git-lab-pagination.enum';
import {CheckUtils} from '../../utils/check.utils';

export class GitLabService {

    /**
     * Initialize default pagination query parameters
     *
     * @param page Page number
     * @param pageSize Number of items per page
     *
     * @return Base pagination params
     */
    static initializeHttpParams(page?: number, pageSize?: number): HttpParams {
        let params = new HttpParams();
        params = params.append(GitlabPaginationEnum.PAGE, this.getPage(page).toString());
        params = params.append(GitlabPaginationEnum.PER_PAGE, this.getPageSize(pageSize).toString());
        return params;
    }

    /**
     * Get matching page number for a given page number
     *
     * @param page Page number
     */
    static getPage(page?: number): number {
        if (!CheckUtils.isNullOrUndefined(page)) {
            return (page <= 0) ? 1 : page;
        } else {
            return 1;
        }
    }

    /**
     * Get elements per page number
     *
     * @param pageSize Elements per page
     */
    static getPageSize(pageSize?: number): number {
        if (!CheckUtils.isNullOrUndefined(pageSize)) {
            return (pageSize <= 0) ? GitlabPaginationEnum.DEFAULT_PER_PAGE : pageSize;
        } else {
            return GitlabPaginationEnum.DEFAULT_PER_PAGE;
        }
    }

    /**
     * Get next page number from headers
     *
     * @param headers Headers of the response
     */
    static getNexPage(headers?: HttpHeaders): number {
        const nextPage = Number(headers.get(GitlabPaginationEnum.NEXT_PAGE));
        return (!CheckUtils.isNullOrUndefined(nextPage) && !isNaN(nextPage)) ? nextPage : 0;
    }

    /**
     * Check if next page is last page of pagination
     *
     * @param nextPage Number of nextpage
     */
    static isLastPage(nextPage?: number): boolean {
        return (!CheckUtils.isUndefined(nextPage)) && (nextPage <= 0);
    }

}
