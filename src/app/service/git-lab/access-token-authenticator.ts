/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {Authenticator} from '../authenticator';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class AccessTokenAuthenticator extends Authenticator {
    private static readonly API_EP = 'https://gitlab.com/api/v4';
    public static readonly AUTH_NAME = 'access-token';

    constructor(private http: HttpClient) {
        super();

        const storedToken = AccessTokenAuthenticator.getStoredApiToken();

        if (null !== storedToken && AccessTokenAuthenticator.AUTH_NAME === storedToken.type) {
            this.authenticated = true;
            this.token = storedToken.token;
        }
    }

    /**
     * @inheritDoc
     */
    authenticate(options: { accessToken: string }): Promise<boolean> {
        if (this.isAuthenticated) {
            this.logout();
        }

        return new Promise(((resolve, reject) => {
            const header = new HttpHeaders().set('Private-Token', options.accessToken);
            this.http.get(AccessTokenAuthenticator.API_EP + '/user/status', {headers: header})
                .subscribe(() => {
                    AccessTokenAuthenticator.storeApiToken(options.accessToken, AccessTokenAuthenticator.AUTH_NAME);
                    this.token = options.accessToken;
                    this.authenticated = true;
                    resolve(true);
                }, error => {
                    console.log(error);
                    reject(false);
                });
        }));
    }

    headerKey(): string {
        return 'Private-Token';
    }

    paramKey(): string {
        return 'private_token';
    }

    headerValue(): string {
        return this.apiToken;
    }
}
