/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export enum GitLabPathsEnum {
    BASE_PATH = 'https://gitlab.com/api/v4',
    ISSUES_PATH = 'https://gitlab.com/api/v4/issues',
    MERGE_REQUESTS_PATH = 'https://gitlab.com/api/v4/merge_requests',
    PROJECTS_PATH = 'https://gitlab.com/api/v4/projects',
}
