/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Project} from '../../../entity/project';
import {ObserveEnum} from '../../observe.enum';
import {Label} from '../../../entity/label';
import {GitLabPathsEnum} from '../git-lab-paths.enum';
import {ProjectsEnum} from './projects.enum';
import {GitLabService} from '../git-lab.service';
import {expand} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {RawData} from '../../../model/raw-data';
import {User} from '../../../entity/user';


@Injectable({
    providedIn: 'root'
})

export class ProjectsService {

    constructor(protected httpClient: HttpClient) {
    }

    /**
     * Finds Project where user is a member
     *
     * @param page Page to return
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param pageSize Number of items returned by page
     * @param reportProgress Flag to report request and response progress.
     */
    public find(page?: number, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<Project>>;
    public find(page?: number, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<Project>>>;
    public find(page?: number, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<Project>>>;
    public find(page?: number, observe: any = ObserveEnum.BODY, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        let params = GitLabService.initializeHttpParams(page, pageSize);
        params = params.append(ProjectsEnum.MEMBERSHIP, String(true));
        params = params.append(ProjectsEnum.SIMPLE, String(true));

        return this.httpClient.get<Array<Project>>(GitLabPathsEnum.PROJECTS_PATH,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Finds Project by id
     *
     * @param pId Id of the Project
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress Flag to report request and response progress.
     */
    public get(pId: number, observe?: ObserveEnum.BODY, reportProgress?: boolean): Observable<Project>;
    public get(pId: number, observe?: ObserveEnum.RESPONSE, reportProgress?: boolean): Observable<HttpResponse<Project>>;
    public get(pId: number, observe?: ObserveEnum.EVENTS, reportProgress?: boolean): Observable<HttpEvent<Project>>;
    public get(pId: number, observe: any = ObserveEnum.BODY, reportProgress: boolean = false): Observable<any> {
        let params = new HttpParams();
        params = params.append(ProjectsEnum.SIMPLE, String(true));

        return this.httpClient.get<Array<Project>>(`${GitLabPathsEnum.PROJECTS_PATH}/${pId}`,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Finds labels for Project
     *
     * @param pId Id of the Project
     * @param page Page to return
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param pageSize Number of items returned by page
     * @param reportProgress Flag to report request and response progress.
     */
    public findLabels(pId: number, page?: number, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<Label>>;
    public findLabels(pId: number, page?: number, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<Label>>>;
    public findLabels(pId: number, page?: number, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<Label>>>;
    public findLabels(pId: number, page?: number, observe: any = ObserveEnum.BODY, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        const params = GitLabService.initializeHttpParams(page, pageSize);

        return this.httpClient.get<Array<Label>>(`${GitLabPathsEnum.PROJECTS_PATH}/${pId}/labels`,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Finds members for Project
     *
     * @param pId Id of the Project
     * @param page Page to return
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param pageSize Number of items returned by page
     * @param reportProgress Flag to report request and response progress.
     */
    public findMembers(pId: number, page?: number, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<User>>;
    public findMembers(pId: number, page?: number, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<User>>>;
    public findMembers(pId: number, page?: number, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<User>>>;
    public findMembers(pId: number, page?: number, observe: any = ObserveEnum.BODY, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        const params = GitLabService.initializeHttpParams(page, pageSize);

        return this.httpClient.get<Array<Label>>(`${GitLabPathsEnum.PROJECTS_PATH}/${pId}/members/all`,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Load all projects recursively
     *
     * @param rawData Object to fill
     */
    public getAllProjectsPages(rawData: RawData): Observable<Array<Project>> {
        const getPage = (page?: number): Observable<any> => {
            return this.find(page, ObserveEnum.RESPONSE);
        };

        return getPage().pipe(expand((res: HttpResponse<Array<Project>>) => {
            const nextPage = GitLabService.getNexPage(res.headers);
            rawData.projects = rawData.projects.concat(res.body).filter((item, index, self) => {
                return self.findIndex((other) => other.id === item.id) === index;
            });
            return (GitLabService.isLastPage(nextPage)) ? EMPTY : getPage(nextPage);
        }));
    }

    /**
     * Load all projects labels recursively
     *
     * @param project Object to fill
     */
    public getAllProjectLabelsPages(project: Project): Observable<Array<Label>> {
        const getPage = (page?: number): Observable<any> => {
            return this.findLabels(project.id, page, ObserveEnum.RESPONSE);
        };

        project.labels = [];

        return getPage().pipe(expand((res: HttpResponse<Array<Label>>) => {
            const nextPage = GitLabService.getNexPage(res.headers);
            project.labels = project.labels.concat(res.body).filter((item, index, self) => {
                return self.findIndex((other) => other.id === item.id) === index;
            });
            return (GitLabService.isLastPage(nextPage)) ? EMPTY : getPage(nextPage);
        }));
    }

    /**
     * Load all projects members recursively
     *
     * @param project Object to fill
     */
    public getAllProjectMembersPages(project: Project): Observable<Array<User>> {
        const getPage = (page?: number): Observable<any> => {
            return this.findMembers(project.id, page, ObserveEnum.RESPONSE);
        };

        project.members = [];

        return getPage().pipe(expand((res: HttpResponse<Array<User>>) => {
            const nextPage = GitLabService.getNexPage(res.headers);
            project.members = project.members.concat(res.body).filter((item, index, self) => {
                return self.findIndex((other) => other.id === item.id) === index;
            });
            return (GitLabService.isLastPage(nextPage)) ? EMPTY : getPage(nextPage);
        }));
    }

}
