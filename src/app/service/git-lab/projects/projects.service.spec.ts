/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {ProjectsService} from './projects.service';
import {Project} from '../../../entity/project';
import {GitLabPathsEnum} from '../git-lab-paths.enum';

describe('Service: ProjectsService', () => {
    let injector: TestBed;
    let service: ProjectsService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [ProjectsService]
        });

        injector = getTestBed();
        service = injector.get(ProjectsService);
        httpMock = injector.get(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should return an Observable<Array<Project>>', () => {
        const dummyData: Array<Project> = [
            {
                id: 1,
                description: null,
                name: 'Pizza',
                name_with_namespace: '',
                path: '',
                path_with_namespace: '',
                created_at: '',
                labels: [],
                members: [],
                default_branch: '',
                tag_list: [],
                ssh_url_to_repo: '',
                http_url_to_repo: '',
                web_url: '',
                readme_url: '',
                avatar_url: '',
                star_count: 0,
                forks_count: 0,
                last_activity_at: '',
                namespace: null
            },
            {
                id: 2,
                description: null,
                name: 'Tacos',
                name_with_namespace: '',
                path: '',
                path_with_namespace: '',
                created_at: '',
                labels: [],
                members: [],
                default_branch: '',
                tag_list: [],
                ssh_url_to_repo: '',
                http_url_to_repo: '',
                web_url: '',
                readme_url: '',
                avatar_url: '',
                star_count: 0,
                forks_count: 0,
                last_activity_at: '',
                namespace: null
            }
        ];

        service.find().subscribe(data => {
            expect(data.length).toBe(2);
            expect(data).toEqual(dummyData);
        });

        const testRequest = httpMock.expectOne(req =>
            req.method === 'GET' && req.url === GitLabPathsEnum.PROJECTS_PATH);
        expect(testRequest.request.method).toBe('GET');
        testRequest.flush(dummyData);
    });

    it('should return a Project', () => {
        const dummyData: Project = {
            id: 1,
            description: null,
            name: 'Pizza',
            name_with_namespace: '',
            path: '',
            path_with_namespace: '',
            created_at: '',
            labels: [],
            members: [],
            default_branch: '',
            tag_list: [],
            ssh_url_to_repo: '',
            http_url_to_repo: '',
            web_url: '',
            readme_url: '',
            avatar_url: '',
            star_count: 0,
            forks_count: 0,
            last_activity_at: '',
            namespace: null
        };

        service.get(1).subscribe((data: Project) => {
            expect(data.id).toBe(1);
            expect(data).toEqual(dummyData);
        });

        const testRequest = httpMock.expectOne(req =>
            req.method === 'GET' && req.url === `${GitLabPathsEnum.PROJECTS_PATH}/1`);
        expect(testRequest.request.method).toBe('GET');
        testRequest.flush(dummyData);
    });
});
