/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ObserveEnum} from '../../observe.enum';
import {GitLabPathsEnum} from '../git-lab-paths.enum';
import {Note} from '../../../entity/note';
import {GitLabService} from '../git-lab.service';
import {Issue} from '../../../entity/issue';
import {MergeRequest} from '../../../entity/merge-request';
import {expand} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {RawData} from '../../../model/raw-data';


@Injectable({
    providedIn: 'root'
})

export class NotesService {

    constructor(protected httpClient: HttpClient) {
    }

    /**
     * Finds Notes for Issue
     *
     * @param projectId Id of the Project
     * @param issueIid Id of the Issue
     * @param page Page to return
     * @param pageSize Number of items returned by page
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress Flag to report request and response progress.
     */
    public findNotesForIssue(projectId: number, issueIid: number, page?: number, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<Note>>;
    public findNotesForIssue(projectId: number, issueIid: number, page?: number, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<Note>>>;
    public findNotesForIssue(projectId: number, issueIid: number, page?: number, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<Note>>>;
    public findNotesForIssue(projectId: number, issueIid: number, page: number, observe: any = ObserveEnum.RESPONSE, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        const params = GitLabService.initializeHttpParams(page, pageSize);

        return this.httpClient.get<Array<Issue>>(`${GitLabPathsEnum.PROJECTS_PATH}/${projectId}/issues/${issueIid}/notes`,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Finds Notes for MergeRequest
     *
     * @param projectId Id of the Project
     * @param mrIid Id of the MergeRequest
     * @param page Page to return
     * @param pageSize Number of items returned by page
     * @param observe Set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress Flag to report request and response progress.
     */
    public findNotesForMergeRequest(projectId: number, mrIid: number, page?: number, observe?: ObserveEnum.BODY, pageSize?: number, reportProgress?: boolean): Observable<Array<Note>>;
    public findNotesForMergeRequest(projectId: number, mrIid: number, page?: number, observe?: ObserveEnum.RESPONSE, pageSize?: number, reportProgress?: boolean): Observable<HttpResponse<Array<Note>>>;
    public findNotesForMergeRequest(projectId: number, mrIid: number, page?: number, observe?: ObserveEnum.EVENTS, pageSize?: number, reportProgress?: boolean): Observable<HttpEvent<Array<Note>>>;
    public findNotesForMergeRequest(projectId: number, mrIid: number, page: number, observe: any = ObserveEnum.RESPONSE, pageSize?: number, reportProgress: boolean = false): Observable<any> {
        const params = GitLabService.initializeHttpParams(page, pageSize);

        return this.httpClient.get<Array<MergeRequest>>(`${GitLabPathsEnum.PROJECTS_PATH}/${projectId}/merge_requests/${mrIid}/notes`,
            {params: params, observe: observe, reportProgress: reportProgress}
        );
    }

    /**
     * Load all projects issues recursively
     *
     * @param rawData Object to fill
     * @param projectId Id of the Project
     * @param issueIId IId od the Issue
     *
     * @return Observable<Array<Note>>
     */
    public loadIssuesNotesPages(rawData: RawData, projectId: number, issueIId: number): Observable<Array<Note>> {
        const getPage = (page?: number): Observable<any> => {
            return this.findNotesForIssue(projectId, issueIId, page);
        };

        return getPage().pipe(expand((res: HttpResponse<Array<Note>>) => {
            const nextPage = GitLabService.getNexPage(res.headers);
            rawData.notes = rawData.notes.concat(res.body).filter((item, index, self) => {
                return self.findIndex((other) => other.id === item.id) === index;
            });
            return (GitLabService.isLastPage(nextPage)) ? EMPTY : getPage(nextPage);
        }));
    }

    /**
     * Load all projects issues recursively
     *
     * @param projectId Id of the Project
     * @param mrIId IId od the MergeRequest
     * @param rawData Object to fill
     *
     * @return Observable<Array<Note>>
     */
    public loadMergeRequestNotesPages(rawData: RawData, projectId: number, mrIId: number): Observable<Array<Note>> {
        const getPage = (page?: number): Observable<any> => {
            return this.findNotesForMergeRequest(projectId, mrIId, page);
        };

        return getPage().pipe(expand((res: HttpResponse<Array<Note>>) => {
            const nextPage = GitLabService.getNexPage(res.headers);
            rawData.notes = rawData.notes.concat(res.body).filter((item, index, self) => {
                return self.findIndex((other) => other.id === item.id) === index;
            });
            return (GitLabService.isLastPage(nextPage)) ? EMPTY : getPage(nextPage);
        }));
    }
}
