/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export enum GitlabPaginationEnum {
    TOTAL_ITEMS = 'X-Total',
    TOTAL_PAGES = 'X-Total-Pages',
    ITEMS_PER_PAGE = 'X-Per-Page',
    CURRENT_PAGE = 'X-Page',
    NEXT_PAGE = 'X-Next-Page',
    PREV_PAGE = 'X-Prev-Page',
    PAGE = 'page',
    PER_PAGE = 'per_page',
    DEFAULT_PER_PAGE = 200
}
