/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {getTestBed, TestBed} from '@angular/core/testing';
import {GitLabService} from './git-lab.service';
import {HttpParams} from '@angular/common/http';
import {GitlabPaginationEnum} from './git-lab-pagination.enum';

describe('Service: GitLabService', () => {
    let injector: TestBed;
    let service: GitLabService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [GitLabService]
        });

        injector = getTestBed();
        service = injector.get(GitLabService);
    });

    it('GitLabService.getPage() should return 1', () => {
        const page = GitLabService.getPage();
        expect(page).toBe(1);
    });

    it('GitLabService.getPage(null) should return 1', () => {
        const page = GitLabService.getPage(null);
        expect(page).toBe(1);
    });

    it('GitLabService.getPage(0) should return 1', () => {
        const page = GitLabService.getPage(0);
        expect(page).toBe(1);
    });

    it('GitLabService.getPage(1) should return 1', () => {
        const page = GitLabService.getPage(1);
        expect(page).toBe(1);
    });

    it('GitLabService.getPage(2) should return 2', () => {
        const page = GitLabService.getPage(2);
        expect(page).toBe(2);
    });

    it('GitLabService.initializeHttpParams() should return HttpParams(page = 1)', () => {
        let dummyParams = new HttpParams();
        dummyParams = dummyParams.append(GitlabPaginationEnum.PAGE, GitLabService.getPage(0).toString());

        const params = GitLabService.initializeHttpParams();
        expect(params.get(GitlabPaginationEnum.PAGE)).toBe(dummyParams.get(GitlabPaginationEnum.PAGE));
    });

    it('GitLabService.initializeHttpParams(null, 5) should return HttpParams(page = 1, per_page = 5)', () => {
        const perPage = 5;
        let dummyParams = new HttpParams();
        dummyParams = dummyParams.append(GitlabPaginationEnum.PAGE, GitLabService.getPage(0).toString());
        dummyParams = dummyParams.append(GitlabPaginationEnum.PER_PAGE, perPage.toString());

        const params = GitLabService.initializeHttpParams(null, perPage);
        expect(params.get(GitlabPaginationEnum.PAGE)).toBe(dummyParams.get(GitlabPaginationEnum.PAGE));
        expect(params.get(GitlabPaginationEnum.PER_PAGE)).toBe(dummyParams.get(GitlabPaginationEnum.PER_PAGE));
    });
});
