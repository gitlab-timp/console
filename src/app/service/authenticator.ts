/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {HeaderProvider} from './header-provider';

export abstract class Authenticator implements HeaderProvider {
    private static readonly TOKEN = 'api-token';
    private static readonly TYPE = 'auth-type';

    protected authenticated = false;
    protected token: string | null = null;

    /**
     * Store api token with authentication type
     *
     * @param token Token to store
     * @param authType Authentication type. Used for auto construction when token is recovered from local storage
     */
    protected static storeApiToken(token: string, authType: string) {
        localStorage.setItem(Authenticator.TOKEN, token);
        localStorage.setItem(Authenticator.TYPE, authType);
    }

    /**
     * Get stored api token and authentication type
     *
     * @return Token and Authentication type or null
     */
    public static getStoredApiToken(): { token: string, type: string } | null {
        const token = localStorage.getItem(Authenticator.TOKEN);
        const type = localStorage.getItem(Authenticator.TYPE);

        if (null === token || null === type) {
            return null;
        }

        return {token: token, type: type};
    }

    /**
     * Remove stored API token and auth type
     */
    protected static removeStoredApiToken() {
        localStorage.removeItem(Authenticator.TOKEN);
        localStorage.removeItem(Authenticator.TYPE);
    }

    /**
     * User is authenticated?
     */
    get isAuthenticated(): boolean {
        return this.authenticated;
    }

    /**
     * Getter for API token
     */
    get apiToken(): string | null {
        if (null === this.token) {
            const storedToken = Authenticator.getStoredApiToken();

            if (null !== storedToken) {
                this.token = storedToken.token;
            }
        }
        return this.token;
    }

    /**
     * Authenticate user
     *
     * @param options Parameters for authentication
     */
    abstract authenticate(options: { [key: string]: string }): Promise<boolean>;

    /**
     * Logout current user and remove stored token
     */
    public logout(): boolean {
        this.authenticated = false;
        Authenticator.removeStoredApiToken();
        return true;
    }

    abstract headerKey(): string;

    abstract paramKey(): string;

    abstract headerValue(): string;
}
