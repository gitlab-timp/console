/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {getTestBed, TestBed} from '@angular/core/testing';
import {AuthProviderService} from './auth-provider.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('Service: AuthProvider', () => {
    let injector: TestBed;
    let service: AuthProviderService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [AuthProviderService]
        });

        injector = getTestBed();
        service = injector.get(AuthProviderService);
    });

    it('should be created', () => {
        // const service: AuthProviderService = TestBed.getAuthenticated(AuthProviderService);
        expect(service).toBeTruthy();
    });
});
