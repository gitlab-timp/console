/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export enum DividerEnum {
    HUMAN_TIME = ' ',
    ALPHAS = ' ',
    DIGITS = ' '
}
