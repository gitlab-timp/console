/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export enum TimeEnum {
    MILLI_SECONDS_PER_DAY = 86400000,
    SECONDS_PER_DAY = 86400,
    SECONDS_PER_HOUR = 3600,
    SECONDS_PER_MINUTE = 60,
    DAYS_PER_WEEK = 7,
    WEEKS_PER_YEAR = 52,
    W = 144000,
    D = 28800,
    H = 3600,
    M = 60,
    W_ALPHA = 'w',
    D_ALPHA = 'd',
    H_ALPHA = 'h',
    M_ALPHA = 'm'
}
