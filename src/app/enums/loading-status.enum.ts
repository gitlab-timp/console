/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export enum LoadingStatusEnum {
    FETCHING_PROJECTS = 'Fetching projects',
    FETCHING_ISSUES = 'Fetching issues',
    FETCHING_ISSUES_NOTES = 'Fetching issues notes',
    FETCHING_MERGE_REQUESTS = 'Fetching merge requests',
    FETCHING_MERGE_REQUESTS_NOTES = 'Fetching merge requests notes',
    COMPUTING_LOGS = 'Computing logs'
}
