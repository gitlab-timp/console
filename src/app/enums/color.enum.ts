/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export enum ColorEnum {
    DEFAULT = '#FFFFFF',
    LOG_ENTRY_DEFAULT = '#CCCCCC',
    IID_DEFAULT = '#FF0000',
    TODAY = '#ECEFF1',
    ISSUE = '#F3E5F5',
    IID_ISSUE = '#C0B3C2',
    MERGE_REQUEST = '#E8EAF6',
    IID_MERGE_REQUEST = '#B6B8C3',
    HUE_MAX = 360,
    SATURATION_MAX = 100,
    LIGHTNESS_MAX = 100,
    HUE_MIN = 0,
    SATURATION_MIN = 0,
    LIGHTNESS_MIN = 0,
    R_MIN = 0,
    R_MAX = 255,
    G_MIN = 0,
    G_MAX = 255,
    B_MIN = 0,
    B_MAX = 255,
}
