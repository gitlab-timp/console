/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {AuthProviderService} from '../service/auth-provider.service';
import {Authenticator} from '../service/authenticator';

@Injectable({
    providedIn: 'root'
})
export class AuthHttpInterceptor implements HttpInterceptor {
    private gitlabRegexp = new RegExp('^https://gitlab\.com');
    private readonly authenticator: Authenticator;

    constructor(authProvider: AuthProviderService) {
        this.authenticator = authProvider.authenticator;
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.gitlabRegexp.test(req.url) && null !== this.authenticator && this.authenticator.isAuthenticated) {
            // const headers = {
            // 'Accept': 'application/json',
            // 'Access-Control-Allow-Origin': '*',
            // 'Access-Control-Allow-Credentials': 'true',
            // 'Cache-Control': 'max-age=0, must-revalidate, no-cache, no-store',
            // 'Content-Type': 'application/json; charset=utf-8',
            // 'Pragma': 'no-cache'
            // };

            // headers[this.authenticator.headerKey()] = this.authenticator.headerValue();
            // req = req.clone({setHeaders: headers});

            req = req.clone({params: req.params.append(this.authenticator.paramKey(), this.authenticator.headerValue())});
        }

        return next.handle(req);
    }

}
