/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {TimeLog} from './time-log';
import {MemberTime} from './member-time';
import {CheckUtils} from '../utils/check.utils';
import {User} from '../entity/user';

export class LabelTime {
    label_id: number;
    label_name: string;
    members_times: Array<MemberTime>;

    /**
     * Constructor LegendEntry
     *
     * @param labelId Id of the label
     * @param labelName Name of the label
     * @param members Members of label time
     */
    constructor(labelId: number, labelName: string, members: Array<User>) {
        this.label_id = labelId;
        this.label_name = labelName;
        this.members_times = [];

        // initialize array foreach member
        for (const m of members) {
            this.members_times.push(new MemberTime(m.id, m.name));
        }
    }

    /**
     * Add time entry
     *
     * @param timeLog TimeLog object to add
     */
    public addTime(timeLog: TimeLog) {
        const userTimes = this.members_times.filter(item => item.member_id === timeLog.author_id);

        if (CheckUtils.isEmpty(userTimes)) {
            this.members_times.push(MemberTime.createFromTimeLog(timeLog));
        } else {
            userTimes[0].time_logs.push(timeLog);
        }
    }

    /**
     * Compute total time logged
     */
    public getTotalTimeLogged() {
        return this.members_times.map(item => item.getTotalTimeLogged()).reduce((a, b) => a + b, 0);
    }

}
