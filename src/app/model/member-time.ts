/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {TimeLog} from './time-log';

export class MemberTime {
    member_id: number;
    member_name: string;
    time_logs: Array<TimeLog>;

    /**
     * Constructor LegendEntry
     * @param memberId Id of the user
     * @param memberName Name of the user
     */
    constructor(memberId: number, memberName: string) {
        this.member_id = memberId;
        this.member_name = memberName;
        this.time_logs = [];
    }

    /**
     * Create a user label time from time log object
     *
     * @param timeLog TimeLog object
     */
    static createFromTimeLog(timeLog: TimeLog): MemberTime {
        const obj = new MemberTime(timeLog.author_id, timeLog.author_name);
        obj.time_logs.push(timeLog);
        return obj;
    }

    /**
     * Compute total time logged
     */
    public getTotalTimeLogged() {
        return this.time_logs.map(item => item.time_logged).reduce((a, b) => a + b, 0);
    }

}
