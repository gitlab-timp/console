/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {TimeLog} from './time-log';
import {NoteableTypeEnum} from '../enums/noteable-type.enum';

describe('Model: TimeLog', () => {
    let model: TimeLog;

    beforeEach(() => {
        model = new TimeLog(
            1,
            1,
            'Test',
            new Date().toISOString(),
            1,
            'Test/Test',
            1,
            NoteableTypeEnum.ISSUE,
            'Test',
            3000,
            new Date(),
            false,
            []
        );
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          TESTS
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('LegendEntry(test, ColorEnum.DEFAULT) should color = ColorEnum.DEFAULT', () => {
        // expect(model.color).toBe(ColorEnum.DEFAULT);
        // expect(model.tag).toBe('test');
        expect(true).toBe(true);
    });

});
