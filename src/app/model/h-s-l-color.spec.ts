/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {HSLColor} from './h-s-l-color';
import {RGBColor} from './r-g-b-color';

describe('Model: HSLColor', () => {
    let model: HSLColor;

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          TESTS
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('HSLColor(360, 100, 100) should create', () => {
        const h = 360;
        const s = 100;
        const l = 100;

        model = new HSLColor(h, s, l);

        expect(model.h).toBe(h);
        expect(model.s).toBe(s);
        expect(model.l).toBe(l);
    });

    it('HSLColor(360, 100, 50).toHex should be #FF0000', () => {
        const expected = new RGBColor(255, 0, 0);
        model = new HSLColor(360, 100, 50);

        expect(model.toRgb().toHex()).toBe(expected.toHex());
    });

});
