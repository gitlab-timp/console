/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {LoadingStatusEnum} from '../enums/loading-status.enum';

export class LoadingStep {
    done: boolean;
    error: boolean;
    loading: boolean;
    statusMsg: LoadingStatusEnum;

    /**
     * Constructor LoadingStep
     *
     * @param done Done state
     * @param error Error state
     * @param loading Loading state
     * @param statusMsg Message status of loading step
     */
    constructor(statusMsg: LoadingStatusEnum, done: boolean = false, error: boolean = false, loading: boolean = false) {
        this.done = false;
        this.error = false;
        this.loading = false;
        this.statusMsg = statusMsg;
    }

    /**
     * Update all states to match success done state
     */
    public setDone() {
        this.done = true;
        this.error = false;
        this.loading = false;
    }

    /**
     * Update all states to match error done state
     */
    public setError() {
        this.done = false;
        this.error = true;
        this.loading = false;
    }

    /**
     * Update all states to match loading state
     */
    public setLoading() {
        this.done = false;
        this.error = false;
        this.loading = true;
    }

    /**
     * Reset all states to false
     */
    public reset() {
        this.done = false;
        this.error = false;
        this.loading = false;
    }

}
