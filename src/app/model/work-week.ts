/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {TimeUtils} from '../utils/time.utils';
import {DaysOfWeekEnum} from '../enums/days-of-week.enum';

export class WorkWeek {
    start: Date;
    end: Date;
    weekNumber: number;

    /**
     * Constructor MyWorkWeek
     *
     * @param start First day of the week
     * @param end Last day of the week
     * @param weekNumber Week number of the week
     */
    constructor(start: Date, end: Date, weekNumber: number) {
        this.start = start;
        this.end = end;
        this.weekNumber = weekNumber;
    }

    /**
     * Create a work week for a date
     *
     * @param cDate Date to create week
     */
    static createForDate(cDate): WorkWeek {
        return new WorkWeek(
            TimeUtils.getDateForDayOfWeek(cDate, DaysOfWeekEnum.MONDAY),
            TimeUtils.getDateForDayOfWeek(cDate, DaysOfWeekEnum.SUNDAY),
            TimeUtils.getWeekNumber(cDate)
        );
    }

}
