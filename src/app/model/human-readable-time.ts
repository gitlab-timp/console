/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {LocaleEnum} from '../enums/locale.enum';
import {LocaleUtils} from '../utils/locale.utils';
import {TimeEnum} from '../enums/time.enum';

export class HumanReadableTime {
    minutes: number;
    hours: number;

    /**
     * Constructor HumanReadableTime
     *
     * @param s Number of seconds
     */
    constructor(s: number) {
        this.hours = Math.floor(s / TimeEnum.SECONDS_PER_HOUR);
        const hoursRest = s % TimeEnum.SECONDS_PER_HOUR;
        this.minutes = Math.floor(hoursRest / TimeEnum.SECONDS_PER_MINUTE);
    }

    /**
     * Get printable human readable time
     *
     * @param locale Locale of the user
     */
    public getPrintable(locale: LocaleEnum = LocaleEnum.EN): string {
        const printableData: Array<string> = [];

        if (this.hours > 0) {
            printableData.push(`${this.hours}${LocaleUtils.getHoursForLocale(locale)}`);
        }

        if (this.minutes > 0) {
            printableData.push(`${this.minutes}${LocaleUtils.getMinutesForLocale(locale)}`);
        }

        return printableData.join(' ');
    }

    /**
     * Get printable human readable time
     *
     * @param locale Locale of the user
     */
    public getPrintableTotal(locale: LocaleEnum = LocaleEnum.EN): string {
        return [
            `${this.hours}${LocaleUtils.getHoursForLocale(locale)}`,
            `${this.minutes}${LocaleUtils.getMinutesForLocale(locale)}`
        ].join(' ');
    }

}
