/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {MyWorkDay} from './my-work-day';
import {TimeLog} from './time-log';
import {NoteableTypeEnum} from '../enums/noteable-type.enum';
import {ColorEnum} from '../enums/color.enum';
import {LocaleEnum} from '../enums/locale.enum';
import {HumanReadableTime} from './human-readable-time';

describe('Model: MyWorkDay', () => {
    let model: MyWorkDay;
    let date: Date;
    let logs: Array<TimeLog>;
    let altDate: Date;

    beforeEach(() => {
        date = new Date();
        altDate = new Date('2018-10-10');

        logs = [
            new TimeLog(
                1,
                1,
                'Test',
                new Date().toISOString(),
                1,
                'Test/Test',
                1,
                NoteableTypeEnum.ISSUE,
                'Test',
                3700,
                new Date(),
                false,
                []
            )
        ];

        model = new MyWorkDay(date, logs);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          TESTS
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('MyWorkDay(date, logs) should create', () => {
        expect(model.date).toBe(date);
        expect(model.logs).toBe(logs);
    });

    it('MyWorkDay(date, logs) should.getBackgroundColor should be ColorEnum.TODAY', () => {
        expect(model.getBackgroundColor()).toBe(ColorEnum.TODAY);
    });

    it('MyWorkDay(2018-10-10, logs).getBackgroundColor should be ColorEnum.DEFAULT', () => {
        model.date = altDate;
        expect(model.getBackgroundColor()).toBe(ColorEnum.DEFAULT);
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(EN) should be wednesday', () => {
        model.date = altDate;
        expect(model.getWeekDayName(LocaleEnum.EN).toLowerCase()).toBe('wednesday'.toLowerCase());
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(FR) should be mercredi', () => {
        model.date = altDate;
        expect(model.getWeekDayName(LocaleEnum.FR).toLowerCase()).toBe('mercredi'.toLowerCase());
    });

    it('MyWorkDay(date, logs).getHumanReadableTimeTotal should be 1h 1m', () => {
        const expectedRes = new HumanReadableTime(3700);
        const res = model.getHumanReadableTimeTotal();

        expect(expectedRes.minutes).toBe(res.minutes);
        expect(expectedRes.hours).toBe(res.hours);
        expect(expectedRes.getPrintable()).toBe(res.getPrintable());
    });

    it('MyWorkDay(date, logs).getHumanReadableTimeTotal should be 56m', () => {
        model.logs.push(new TimeLog(
            1,
            1,
            'Test',
            new Date().toISOString(),
            1,
            'Test/Test',
            1,
            NoteableTypeEnum.ISSUE,
            'Test',
            300,
            new Date(),
            true,
            []
        ));

        const expectedRes = new HumanReadableTime(3400);
        const res = model.getHumanReadableTimeTotal();

        expect(expectedRes.minutes).toBe(res.minutes);
        expect(expectedRes.hours).toBe(res.hours);
        expect(expectedRes.getPrintable()).toBe(res.getPrintable());
    });

});
