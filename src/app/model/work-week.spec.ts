/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {WorkWeek} from './work-week';

describe('Model: WorkWeek', () => {
    let model: WorkWeek;

    beforeEach(() => {
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          TESTS
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('WorkWeek.createForDate(2018-12-04) should create', () => {
        model = WorkWeek.createForDate(new Date('2018-12-04'));
        expect(model.weekNumber).toBe(49);
        expect(model.start.getDate()).toBe(new Date('2018-12-03').getDate());
        expect(model.end.getDate()).toBe(new Date('2018-12-09').getDate());
    });

});
