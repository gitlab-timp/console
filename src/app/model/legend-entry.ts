/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {ColorEnum} from '../enums/color.enum';

export class LegendEntry {
    tag: string;
    color: ColorEnum;

    /**
     * Constructor LegendEntry
     * @param tag Tag of legend
     * @param color Color of legend
     */
    constructor(tag: string, color: ColorEnum) {
        this.tag = tag;
        this.color = color;
    }

}
