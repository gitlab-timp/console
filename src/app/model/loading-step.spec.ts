/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {LegendEntry} from './legend-entry';
import {ColorEnum} from '../enums/color.enum';

describe('Model: LegendEntry', () => {
    let model: LegendEntry;

    beforeEach(() => {
        model = new LegendEntry('test', ColorEnum.DEFAULT);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          TESTS
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('LegendEntry(test, ColorEnum.DEFAULT) should color = ColorEnum.DEFAULT', () => {
        // expect(model.color).toBe(ColorEnum.DEFAULT);
        // expect(model.tag).toBe('test');
        expect(true).toBe(true);
    });

});
