/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {RGBColor} from './r-g-b-color';
import {ColorEnum} from '../enums/color.enum';

export class HSLColor {
    h: number;
    s: number;
    l: number;

    /**
     * Constructor RGBColor
     *
     * @param h Hue value
     * @param s Saturation value
     * @param l Lightness value
     */
    constructor(h: number, s: number, l: number) {
        const norm = (x: number, min: number, max: number) => {
            if (x >= max) {
                return max;
            }

            if (x <= min) {
                return min;
            }

            return x;
        };

        this.h = norm(Math.round(h), ColorEnum.HUE_MIN, ColorEnum.HUE_MAX);
        this.s = norm(Math.round(s), ColorEnum.SATURATION_MIN, ColorEnum.SATURATION_MAX);
        this.l = norm(Math.round(l), ColorEnum.LIGHTNESS_MIN, ColorEnum.LIGHTNESS_MAX);
    }

    /**
     * Convert hue value to rgb
     *
     * @param p Transformed Lightness
     * @param q Transformed Saturation
     * @param t Transformed Hue
     */
    private static hue2rgb(p: number, q: number, t: number): number {
        if (t < 0) {
            t += 1;
        }

        if (t > 1) {
            t -= 1;
        }

        if (t < 1 / 6) {
            return p + (q - p) * 6 * t;
        }

        if (t < 1 / 2) {
            return q;
        }

        if (t < 2 / 3) {
            return p + (q - p) * (2 / 3 - t) * 6;
        }

        return p;
    }

    /**
     * Converts an HSL color value to RGB. Conversion formula
     * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
     * Assumes h, s, and l are contained in the set [0, 1] and
     * returns r, g, and b in the set [0, 255].
     *
     * @return  RGBColor  The RGB representation
     */
    public toRgb(): RGBColor {
        const hNorm = this.h / ColorEnum.HUE_MAX;
        const sNorm = this.s / ColorEnum.SATURATION_MAX;
        const lNorm = this.l / ColorEnum.LIGHTNESS_MAX;
        let r, g, b;

        if (sNorm === 0) {
            r = g = b = lNorm; // achromatic
        } else {
            const q = lNorm < 0.5 ? lNorm * (1 + sNorm) : lNorm + sNorm - lNorm * sNorm;
            const p = 2 * lNorm - q;

            r = HSLColor.hue2rgb(p, q, hNorm + 1 / 3);
            g = HSLColor.hue2rgb(p, q, hNorm);
            b = HSLColor.hue2rgb(p, q, hNorm - 1 / 3);
        }

        return new RGBColor(Math.round(r * 255), Math.round(g * 255), Math.round(b * 255));
    }

    /**
     * Convert HSL Color to hex
     */
    public toHex(): string {
        return this.toRgb().toHex();
    }

}
