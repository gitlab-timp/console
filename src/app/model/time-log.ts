/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {NoteableTypeEnum} from '../enums/noteable-type.enum';
import {ColorEnum} from '../enums/color.enum';
import {CheckUtils} from '../utils/check.utils';
import {HumanReadableTime} from './human-readable-time';

export class TimeLog {
    note_id: number;
    author_id: number;
    author_name: string;
    created_at: Date;
    noteable_id: number;
    project_path: string;
    noteable_iid: number;
    noteable_type: string;
    noteable_title: string;
    time_logged: number;
    date_of_log: Date;
    substract: boolean;
    labels: Array<string>;

    /**
     * Constructor TimeLog
     *
     * @param noteId Id of the note
     * @param authorId Id of the author
     * @param authorName Name of the author
     * @param createdAt Date of note creation
     * @param noteableId Id of the project
     * @param projectPath Path of the project
     * @param noteableIid Iid of noteable
     * @param noteableType Type of noteable
     * @param noteableTitle Title of noteable
     * @param timeLogged Amount of seconds logged
     * @param dateOfLog Date of the work
     * @param substract Indicates if it a time substraction
     * @param labels Array of labels names
     */
    constructor(
        noteId: number,
        authorId: number,
        authorName: string,
        createdAt: string,
        noteableId: number,
        projectPath: string,
        noteableIid: number,
        noteableType: string,
        noteableTitle: string,
        timeLogged: number,
        dateOfLog: Date,
        substract: boolean,
        labels: Array<string>
    ) {
        this.note_id = noteId;
        this.author_id = authorId;
        this.author_name = authorName;
        this.created_at = new Date(createdAt);
        this.noteable_id = noteableId;
        this.project_path = projectPath;
        this.noteable_iid = noteableIid;
        this.noteable_type = noteableType;
        this.noteable_title = noteableTitle;
        this.time_logged = timeLogged;
        this.date_of_log = dateOfLog;
        this.substract = substract;
        this.labels = labels;
    }

    /**
     * Return color assotiated to Noteable type
     */
    public getNoteableColor(): string {
        const colors: Array<string> = [];
        colors[NoteableTypeEnum.ISSUE] = ColorEnum.ISSUE;
        colors[NoteableTypeEnum.MERGE_REQUEST] = ColorEnum.MERGE_REQUEST;
        const result = colors[this.noteable_type];
        return CheckUtils.isNullOrUndefined(result) ? ColorEnum.LOG_ENTRY_DEFAULT : result;
    }

    /**
     * Return color assotiated to Noteable type for IID
     */
    public getIIDColor(): string {
        const colors: Array<string> = [];
        colors[NoteableTypeEnum.ISSUE] = ColorEnum.IID_ISSUE;
        colors[NoteableTypeEnum.MERGE_REQUEST] = ColorEnum.IID_MERGE_REQUEST;
        const result = colors[this.noteable_type];
        return CheckUtils.isNullOrUndefined(result) ? ColorEnum.IID_DEFAULT : result;
    }

    /**
     * Return human readable time
     */
    public getHumanReadableTime(): HumanReadableTime {
        return new HumanReadableTime(this.time_logged);
    }

    /**
     * Update time logged of object by coimputing with another log for the same author, date and noteable
     *
     * @param tl TimeLog to update
     */
    public updateTime(tl: TimeLog) {
        if ((this.substract && tl.substract === false) || ((this.substract === false && tl.substract))) {
            this.time_logged = this.time_logged - tl.time_logged;
        }

        if ((this.substract && tl.substract) || ((this.substract === false && tl.substract === false))) {
            this.time_logged = this.time_logged + tl.time_logged;
        }
    }

}
