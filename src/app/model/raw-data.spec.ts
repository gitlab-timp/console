/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {RawData} from './raw-data';

describe('Model: RawData', () => {
    let model: RawData;

    beforeEach(() => {
        model = new RawData();
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          TESTS
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('LegendEntry(test, ColorEnum.DEFAULT) should color = ColorEnum.DEFAULT', () => {
        // expect(model.color).toBe(ColorEnum.DEFAULT);
        // expect(model.tag).toBe('test');
        expect(true).toBe(true);
    });

});
