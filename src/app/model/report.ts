/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class Report {
    id: number;
    name: string;

    /**
     * Constructor Report
     *
     * @param id Id of the report
     * @param name Name of the report
     */
    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }

}
