/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {HumanReadableTime} from './human-readable-time';

describe('Model: HumanReadableTime', () => {
    let model: HumanReadableTime;
    let altModel: HumanReadableTime;

    beforeEach(() => {
        model = new HumanReadableTime(3900);
        altModel = new HumanReadableTime(600);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          TESTS
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('HumanReadableTime(3900) should create', () => {
        expect(model.minutes).toBe(5);
        expect(model.hours).toBe(1);
    });

    it('HumanReadableTime(600) should create', () => {
        expect(altModel.minutes).toBe(10);
        expect(altModel.hours).toBe(0);
    });

    it('HumanReadableTime(3900).getPrintable() should return 1h 5m', () => {
        expect(model.getPrintable()).toBe('1h 5m');
    });

    it('HumanReadableTime(600).getPrintable() should return 10m', () => {
        expect(altModel.getPrintable()).toBe('10m');
    });

});
