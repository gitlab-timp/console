/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {ColorEnum} from '../enums/color.enum';

export class RGBColor {
    r: number;
    g: number;
    b: number;

    /**
     * Constructor RGBColor
     *
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     */
    constructor(r: number, g: number, b: number) {
        const norm = (x: number, min: number, max: number) => {
            if (x >= max) {
                return max;
            }

            if (x <= min) {
                return min;
            }

            return x;
        };

        this.r = norm(Math.round(r), ColorEnum.R_MIN, ColorEnum.R_MAX);
        this.g = norm(Math.round(g), ColorEnum.G_MIN, ColorEnum.G_MAX);
        this.b = norm(Math.round(b), ColorEnum.B_MIN, ColorEnum.B_MAX);
    }

    /**
     * Convert RGB Color to hex
     */
    public toHex(): string {
        const toHex = (x) => {
            const hex = Math.round(x).toString(16);
            return hex.length === 1 ? '0' + hex : hex;
        };

        return `#${toHex(this.r)}${toHex(this.g)}${toHex(this.b)}`.toUpperCase();
    }

}
