/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Project} from '../entity/project';
import {Issue} from '../entity/issue';
import {MergeRequest} from '../entity/merge-request';
import {Note} from '../entity/note';
import {TimeLog} from './time-log';
import {RegexUtils} from '../utils/regex.utils';
import {TimeUtils} from '../utils/time.utils';
import {NoteableTypeEnum} from '../enums/noteable-type.enum';
import {ConstantsEnum} from '../enums/constants.enum';
import {CheckUtils} from '../utils/check.utils';
import {User} from '../entity/user';
import {Label} from '../entity/label';

export class RawData {
    projects: Array<Project>;
    issues: Array<Issue>;
    mergeRequests: Array<MergeRequest>;
    notes: Array<Note>;

    /**
     * Constructor RawData
     */
    constructor() {
        this.projects = [];
        this.issues = [];
        this.mergeRequests = [];
        this.notes = [];
    }

    /**
     * Get Issue matching Iid
     *
     * @param issueId Id of the Issue
     */
    public getIssue(issueId: number): Issue {
        const data: Array<Issue> = this.issues.filter((d: Issue) => {
            return (d.id === issueId);
        });

        return (data.length === 1) ? data[0] : null;
    }

    /**
     * Get MergeRequest matching Iid
     *
     * @param mergeRequestId Id of the MergeRequest
     */
    public getMergeRequest(mergeRequestId: number): MergeRequest {
        const data: Array<MergeRequest> = this.mergeRequests.filter((d: MergeRequest) => {
            return (d.id === mergeRequestId);
        });

        return (data.length === 1) ? data[0] : null;
    }

    /**
     * Get project
     *
     * @param projectId Id of the project
     */
    public getProject(projectId: number): Project {
        const data: Array<Project> = this.projects.filter((d: Project) => {
            return (d.id === projectId);
        });

        return (data.length === 1) ? data[0] : null;
    }

    /**
     * Get title of noteable
     *
     * @param noteableId Id of the noteable
     * @param noteableIid Iid of the noteable
     * @param noteableType Type of the noteable
     */
    public getProjectPath(noteableId: number, noteableIid: number, noteableType: string): string {
        let projectId: number = null;

        switch (noteableType) {
            case NoteableTypeEnum.ISSUE:
                const issue: Issue = this.getIssue(noteableId);
                projectId = CheckUtils.isNullOrUndefined(issue) ? null : issue.project_id;
                break;
            case  NoteableTypeEnum.MERGE_REQUEST:
                const mergeRequest: MergeRequest = this.getMergeRequest(noteableId);
                projectId = CheckUtils.isNullOrUndefined(mergeRequest) ? null : mergeRequest.project_id;
                break;
            default:
                return ConstantsEnum.NULL;
        }

        if (CheckUtils.isNullOrUndefined(projectId)) {
            return ConstantsEnum.NULL;
        } else {
            const project: Project = this.getProject(projectId);
            return (CheckUtils.isNullOrUndefined(project)) ? ConstantsEnum.NULL : project.path_with_namespace;
        }
    }

    /**
     * Get title of noteable
     *
     * @param noteableId Id of the noteable
     * @param noteableIid Iid of the noteable
     * @param noteableType Type of the noteable
     */
    public getNoteableTitle(noteableId: number, noteableIid: number, noteableType: string): string {
        switch (noteableType) {
            case NoteableTypeEnum.ISSUE:
                const issue: Issue = this.getIssue(noteableId);
                return (CheckUtils.isNullOrUndefined(issue)) ? ConstantsEnum.NULL : issue.title;
            case  NoteableTypeEnum.MERGE_REQUEST:
                const mergeRequest: MergeRequest = this.getMergeRequest(noteableId);
                return (CheckUtils.isNullOrUndefined(mergeRequest)) ? ConstantsEnum.NULL : mergeRequest.title;
            default:
                return ConstantsEnum.NULL;
        }
    }

    /**
     * Get title of noteable
     *
     * @param projectId Id of the project
     * @param noteableIid Iid of thje noteable
     * @param noteableType Type of the noteable
     */
    public getNoteableLabels(projectId: number, noteableIid: number, noteableType: string): Array<string> {
        switch (noteableType) {
            case NoteableTypeEnum.ISSUE:
                const issue: Issue = this.getIssue(projectId);
                return (CheckUtils.isNullOrUndefined(issue)) ? [] : issue.labels;
            case  NoteableTypeEnum.MERGE_REQUEST:
                const mergeRequest: MergeRequest = this.getMergeRequest(projectId);
                return (CheckUtils.isNullOrUndefined(mergeRequest)) ? [] : mergeRequest.labels;
            default:
                return [];
        }
    }

    /**
     * Add Time Log to array with computation
     *
     * @param newTL TimeLog to add
     * @param timeLogs Array of time logs
     *
     * @return Array of time logs computed
     */
    private addComputedTimeLog(newTL: TimeLog, timeLogs: Array<TimeLog>): Array<TimeLog> {
        const prevTimeLogs = timeLogs.filter((tl: TimeLog) => {
            return (tl.noteable_id === newTL.noteable_id) && (tl.author_id === newTL.author_id) &&
                (tl.date_of_log.getTime() === newTL.date_of_log.getTime());
        });

        if (prevTimeLogs.length === 0) {
            timeLogs.push(newTL);
        } else {
            prevTimeLogs[0].updateTime(newTL);
        }

        return timeLogs;
    }

    /**
     * Compute time logs for an user
     *
     * @param user Author of time log
     *
     * @return Array of time logs
     */
    public getTimeLogsForUser(user: User): Array<TimeLog> {
        let timeLogs: Array<TimeLog> = [];

        const timeNotes = this.notes.filter((note: Note) => {
            return (note.author.id === user.id) &&
                (RegexUtils.isAddTimeLogNote(note.body) || RegexUtils.isSubtractedTimeLogNote(note.body));
        });

        // Get all times logged and put them in an array where the key is the date (in milliseconds) and the
        // value is an array of all logs of this day
        for (const note of timeNotes) {
            const timeLogged = RegexUtils.extractTimeSpent(note.body);
            const dateLog = RegexUtils.extractDateTimeSpent(note.body);

            if (!CheckUtils.isNullOrUndefined(timeLogged)) {
                timeLogs = this.addComputedTimeLog(new TimeLog(
                    note.id,
                    note.author.id,
                    user.name,
                    note.created_at,
                    note.noteable_id,
                    this.getProjectPath(note.noteable_id, note.noteable_iid, note.noteable_type),
                    note.noteable_iid,
                    note.noteable_type,
                    this.getNoteableTitle(note.noteable_id, note.noteable_iid, note.noteable_type),
                    TimeUtils.getHumanTimeToSeconds(timeLogged),
                    dateLog,
                    RegexUtils.isSubtractedTimeLogNote(note.body),
                    this.getNoteableLabels(note.noteable_id, note.noteable_iid, note.noteable_type)
                ), timeLogs);
            }
        }

        return timeLogs.filter((tl: TimeLog) => {
            return (tl.substract === false) && (tl.time_logged > 0);
        });
    }

    /**
     * Compute time logs for a specific issue and multiple users
     * Filter the results with date
     *
     * @param projectId Id of the project
     * @param users Array of users to compute logs
     * @param issue Issue we want to get the logs from
     * @param fromDate Date of start
     * @param toDate Date of end
     * @return Array of time logs
     */
    public getTimeLogsForProjectIssueByUsers(projectId: number, users: Array<User>, issue: Issue, fromDate: Date, toDate: Date): Array<TimeLog> {
        let timeLogs: Array<TimeLog> = [];

        users.forEach(user => {
            timeLogs = timeLogs.concat(this.getTimeLogsForUser(user));
        });

        // Filter by project
        timeLogs = timeLogs.filter(item => {
            return this.issues.filter(it => it.project_id === projectId).map(it => it.id).includes(item.noteable_id);
        });

        timeLogs = timeLogs.filter(item => issue.id === item.noteable_id);

        // Filter by start date
        if (!CheckUtils.isEmpty(fromDate)) {
            timeLogs = timeLogs.filter(item => item.date_of_log >= fromDate);
        }

        // Filter by end date
        if (!CheckUtils.isEmpty(toDate)) {
            timeLogs = timeLogs.filter(item => item.date_of_log <= toDate);
        }

        return timeLogs;
    }

    /**
     * Compute time logs for users and labels in array and begining after from date and before to date
     *
     * @param projectId Id of the project
     * @param users Array of users to compute logs
     * @param labels Labels to compute logs
     * @param fromDate Date of start
     * @param toDate Date of end
     *
     * @return Array of time logs
     */
    public getTimeLogsProjectLabelsByUser(projectId: number, users: Array<User>, labels: Array<Label>, fromDate?: Date, toDate?: Date): Array<TimeLog> {
        let timeLogs: Array<TimeLog> = [];

        // Filter by users
        const timeNotes = this.notes.filter((note: Note) => {
            return (RegexUtils.isAddTimeLogNote(note.body) || RegexUtils.isSubtractedTimeLogNote(note.body)) &&
                users.map(item => item.id).includes(note.author.id);
        });

        for (const note of timeNotes) {
            const timeLogged = RegexUtils.extractTimeSpent(note.body);
            const dateLog = RegexUtils.extractDateTimeSpent(note.body);

            if (!CheckUtils.isNullOrUndefined(timeLogged)) {
                const timeLogLabels = this.getNoteableLabels(note.noteable_id, note.noteable_iid, note.noteable_type);
                const hasLabel = labels.map(item => item.name).filter(item => -1 !== timeLogLabels.indexOf(item));
                const author = users.filter(item => item.id === note.author.id);
                let authorName = 'Unknow';

                if (!CheckUtils.isEmpty(author)) {
                    authorName = author[0].name;
                }

                // Filter by label
                if (hasLabel.length > 0) {
                    timeLogs = this.addComputedTimeLog(new TimeLog(
                        note.id,
                        note.author.id,
                        authorName,
                        note.created_at,
                        note.noteable_id,
                        this.getProjectPath(note.noteable_id, note.noteable_iid, note.noteable_type),
                        note.noteable_iid,
                        note.noteable_type,
                        this.getNoteableTitle(note.noteable_id, note.noteable_iid, note.noteable_type),
                        TimeUtils.getHumanTimeToSeconds(timeLogged),
                        dateLog,
                        RegexUtils.isSubtractedTimeLogNote(note.body),
                        timeLogLabels
                    ), timeLogs);
                }
            }
        }

        // Remove remaining time substract
        timeLogs = timeLogs.filter(item => item.substract === false && item.time_logged > 0);

        // Filter by project
        timeLogs = timeLogs.filter(item => {
            return this.issues.filter(it => it.project_id === projectId).map(it => it.id).includes(item.noteable_id);
        });

        // Filter by start date
        if (!CheckUtils.isEmpty(fromDate)) {
            timeLogs = timeLogs.filter(item => item.date_of_log >= fromDate);
        }

        // Filter by end date
        if (!CheckUtils.isEmpty(toDate)) {
            timeLogs = timeLogs.filter(item => item.date_of_log <= toDate);
        }

        return timeLogs;
    }

    /**
     * Compute time logs for projet and begining after from date and before to date
     *
     * @param projectId Id of the project
     * @param users Array of users to compute logs
     * @param fromDate Date of start
     * @param toDate Date of end
     *
     * @return Array of time logs
     */
    public getTimeLogsForProject(projectId: number, users: Array<User>, fromDate?: Date, toDate?: Date): Array<TimeLog> {
        let timeLogs: Array<TimeLog> = [];

        // Filter by users
        const timeNotes = this.notes.filter((note: Note) => {
            return (RegexUtils.isAddTimeLogNote(note.body) || RegexUtils.isSubtractedTimeLogNote(note.body));
        });

        for (const note of timeNotes) {
            const timeLogged = RegexUtils.extractTimeSpent(note.body);
            const dateLog = RegexUtils.extractDateTimeSpent(note.body);

            if (!CheckUtils.isNullOrUndefined(timeLogged)) {
                const timeLogLabels = this.getNoteableLabels(note.noteable_id, note.noteable_iid, note.noteable_type);
                const author = users.filter(item => item.id === note.author.id);
                let authorName = 'Unknow';

                if (!CheckUtils.isEmpty(author)) {
                    authorName = author[0].name;
                }

                // Filter by label
                timeLogs = this.addComputedTimeLog(new TimeLog(
                    note.id,
                    note.author.id,
                    authorName,
                    note.created_at,
                    note.noteable_id,
                    this.getProjectPath(note.noteable_id, note.noteable_iid, note.noteable_type),
                    note.noteable_iid,
                    note.noteable_type,
                    this.getNoteableTitle(note.noteable_id, note.noteable_iid, note.noteable_type),
                    TimeUtils.getHumanTimeToSeconds(timeLogged),
                    dateLog,
                    RegexUtils.isSubtractedTimeLogNote(note.body),
                    timeLogLabels
                ), timeLogs);
            }
        }

        // Remove remaining time substract
        timeLogs = timeLogs.filter(item => item.substract === false && item.time_logged > 0);

        // Filter by project
        timeLogs = timeLogs.filter(item => {
            return this.issues.filter(it => it.project_id === projectId).map(it => it.id).includes(item.noteable_id);
        });

        // Filter by start date
        if (!CheckUtils.isEmpty(fromDate)) {
            timeLogs = timeLogs.filter(item => item.date_of_log >= fromDate);
        }

        // Filter by end date
        if (!CheckUtils.isEmpty(toDate)) {
            timeLogs = timeLogs.filter(item => item.date_of_log <= toDate);
        }

        return timeLogs;
    }

}
