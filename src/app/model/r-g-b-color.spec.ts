/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {RGBColor} from './r-g-b-color';

describe('Model: RGBColor', () => {
    let model: RGBColor;

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          TESTS
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('RGBColor(255, 255, 255) should create', () => {
        model = new RGBColor(255, 255, 255);

        expect(model.r).toBe(255);
        expect(model.g).toBe(255);
        expect(model.b).toBe(255);
    });

    it('RGBColor(255, 255, 255).toHex should be #FFFFFF', () => {
        model = new RGBColor(255, 255, 255);
        const expected = '#FFFFFF';

        expect(model.toHex()).toBe(expected);
    });

});
