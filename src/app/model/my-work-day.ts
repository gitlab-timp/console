/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {TimeLog} from './time-log';
import {TimeUtils} from '../utils/time.utils';
import {ColorEnum} from '../enums/color.enum';
import {LocaleEnum} from '../enums/locale.enum';
import {HumanReadableTime} from './human-readable-time';

export class MyWorkDay {
    date: Date;
    logs: Array<TimeLog>;
    total_logged: number;

    /**
     * Constructor MyWorkDay
     *
     * @param date Date of the day
     * @param logs List of logs of the day
     */
    constructor(date: Date, logs: Array<TimeLog> = []) {
        this.date = date;
        this.logs = logs;
        this.computeTotalLogged();
    }

    /**
     * Compute total time logged
     */
    private computeTotalLogged() {
        this.total_logged = 0;

        for (const l of this.logs) {
            if (l.substract) {
                this.total_logged -= l.time_logged;
            } else {
                this.total_logged += l.time_logged;
            }
        }
    }

    /**
     * Return human readable total time logged
     */
    public getHumanReadableTimeTotal(): HumanReadableTime {
        this.computeTotalLogged();
        return new HumanReadableTime(this.total_logged);
    }

    /**
     * Return background color of component
     */
    public getBackgroundColor(): string {
        return TimeUtils.isToday(this.date) ? ColorEnum.TODAY : ColorEnum.DEFAULT;
    }

    /**
     * Return week day name for day date
     *
     * @param locale Locale of the name
     */
    public getWeekDayName(locale: LocaleEnum = LocaleEnum.EN): string {
        return TimeUtils.getWeekDayName(this.date, locale);
    }
}
