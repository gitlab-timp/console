/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


import {ColorEnum} from '../enums/color.enum';
import {HSLColor} from '../model/h-s-l-color';

export class ColorUtils {

    /**
     * Return an array with n random colors Hex format
     *
     * @param nbColors Number of colors
     *
     * @return Array of n random colors Hex format
     */
    static getRandomColorsHex(nbColors: number): Array<string> {
        const colors: Array<string> = [];
        const rand = (min: number, max: number) => {
            return min + Math.random() * (max - min);
        };

        for (let i = 0; i < ColorEnum.HUE_MAX; i += ColorEnum.HUE_MAX / nbColors) {
            colors.push(new HSLColor(i, rand(ColorEnum.SATURATION_MAX / 2, ColorEnum.SATURATION_MAX), ColorEnum.LIGHTNESS_MAX / 2).toHex());
        }

        return colors;
    }

}
