/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {ColorUtils} from './color.utils';

describe('Utils: Color', () => {

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  isEmpty
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('ColorUtils.getRandomColorsHex(10) should return 10', () => {
        const input = 10;
        const result = ColorUtils.getRandomColorsHex(10);
        expect(result.length).toBe(input);
    });

});
