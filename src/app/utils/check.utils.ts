/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class CheckUtils {

    /**
     * Check if variable is empty
     *
     * @param variable Any variable to check
     */
    static isEmpty(variable?: any): boolean {
        return this.isNullOrUndefined(variable) ||
            (this.isString(variable) && variable === '') ||
            (Array.isArray(variable) && variable.length === [].length) ||
            (this.isObject(variable) && JSON.stringify(variable) === JSON.stringify({}));
    }

    /**
     * Check if variable is null
     *
     * @param variable Any variable to check
     */
    static isNull(variable?: any): boolean {
        return variable === null;
    }

    /**
     * Check if variable is null or undefined
     *
     * @param variable Any variable to check
     */
    static isNullOrUndefined(variable?: any): boolean {
        return this.isNull(variable) || this.isUndefined(variable);
    }

    /**
     * Check if variable is an object
     *
     * @param variable Any variable to check
     */
    static isObject(variable?: any): boolean {
        return !this.isNullOrUndefined(variable) && !Array.isArray(variable) && (typeof variable === 'object');
    }

    /**
     * Check if variable is a string
     *
     * @param variable Any variable to check
     */
    static isString(variable?: any): boolean {
        return !this.isNullOrUndefined(variable) && (typeof variable === 'string');
    }

    /**
     * Check if variable is null
     *
     * @param variable Any variable to check
     */
    static isUndefined(variable?: any): boolean {
        return variable === undefined;
    }

}
