/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {NotificationTypeEnum} from '../enums/notification-type.enum';
import {PlacementEnum} from '../enums/placement.enum';

declare let $: any;

export class NotificationUtils {

    /**
     * Show notification on top - center placement
     *
     * @param type Color of notification
     * @param msg Message to display
     * @param icon Icon to display
     * @param duration Time to close automaticly
     * @param from Vertical position
     * @param align Horizontal position
     */
    static showNotification(type: NotificationTypeEnum, msg: string, icon?: string, duration: number = 3000, from: string = PlacementEnum.TOP, align: string = PlacementEnum.CENTER) {
        $.notify({
            icon: icon,
            message: msg
        }, {
            type: type,
            timer: duration,
            placement: {
                from: from,
                align: align
            }
        });
    }
}
