/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {TimeUtils} from './time.utils';
import {DaysOfWeekEnum} from '../enums/days-of-week.enum';
import {TimeEnum} from '../enums/time.enum';
import {LocaleEnum} from '../enums/locale.enum';

describe('Utils: Time', () => {

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getWeekNumber
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('TimeUtils.getWeekNumber(2018-10-24) should return 43', () => {
        const input = new Date('2018-10-24');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 43;
        expect(output).toBe(expected);
    });

    it('TimeUtils.getWeekNumber(2018-10-28) should return 43', () => {
        const input = new Date('2018-10-28');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 43;
        expect(output).toBe(expected);
    });

    it('TimeUtils.getWeekNumber(2018-12-31) should return 1', () => {
        const input = new Date('2018-12-31');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 1;
        expect(output).toBe(expected);
    });

    it('TimeUtils.getWeekNumber(2019-01-01) should return 1', () => {
        const input = new Date('2019-01-01');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 1;
        expect(output).toBe(expected);
    });

    it('TimeUtils.getWeekNumber(2019-01-09) should return 2', () => {
        const input = new Date('2019-01-09');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 2;
        expect(output).toBe(expected);
    });

    it('TimeUtils.getWeekNumber(2017-08-24) should return 34', () => {
        const input = new Date('2017-08-24');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 34;
        expect(output).toBe(expected);
    });

    it('TimeUtils.getWeekNumber(2018-12-24) should return 52', () => {
        const input = new Date('2018-12-24');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 52;
        expect(output).toBe(expected);
    });

    it('TimeUtils.getWeekNumber(2018-12-30) should return 52', () => {
        const input = new Date('2018-12-30');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 52;
        expect(output).toBe(expected);
    });

    it('TimeUtils.getWeekNumber(2018-12-02) should return 48', () => {
        const input = new Date('2018-12-02');
        const output = TimeUtils.getWeekNumber(input);
        const expected = 48;
        expect(output).toBe(expected);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getDateForDayOfWeek
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('TimeUtils.getDateForDayOfWeek(2018-10-24, MONDAY) should return 2018-10-22', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-22');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.MONDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-10-24, TUESDAY) should return 2018-10-23', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-23');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.TUESDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-10-24, WEDNESDAY) should return 2018-10-24', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-24');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.WEDNESDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-10-24, THURSDAY) should return 2018-10-25', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-25');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.THURSDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-10-24, FRIDAY) should return 2018-10-26', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-26');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.FRIDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-10-24, SATURDAY) should return 2018-10-27', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-27');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.SATURDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-10-24, SUNDAY) should return 2018-10-28', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-28');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.SUNDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-12-02, SUNDAY) should return 2018-12-02', () => {
        const input = new Date('2018-12-02');
        const expected = new Date('2018-12-02');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.SUNDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-12-02, MONDAY) should return 2018-11-26', () => {
        const input = new Date('2018-12-02');
        const expected = new Date('2018-11-26');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.MONDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-12-02, MONDAY) should return 2018-11-26', () => {
        const input = new Date('2018-12-02');
        const expected = new Date('2018-11-27');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.TUESDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-12-02, MONDAY) should return 2018-11-26', () => {
        const input = new Date('2018-12-02');
        const expected = new Date('2018-11-28');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.WEDNESDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-12-02, MONDAY) should return 2018-11-26', () => {
        const input = new Date('2018-12-02');
        const expected = new Date('2018-11-29');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.THURSDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-12-02, MONDAY) should return 2018-11-26', () => {
        const input = new Date('2018-12-02');
        const expected = new Date('2018-11-30');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.FRIDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getDateForDayOfWeek(2018-12-02, MONDAY) should return 2018-11-26', () => {
        const input = new Date('2018-12-02');
        const expected = new Date('2018-12-01');
        const output = TimeUtils.getDateForDayOfWeek(input, DaysOfWeekEnum.SATURDAY);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getPreviousWeek
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('TimeUtils.getPreviousWeek(2018-10-24) should return 2018-10-17', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-17');
        const output = TimeUtils.getPreviousWeek(input);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getPreviousWeek(2018-10-03) should return 2018-09-26', () => {
        const input = new Date('2018-10-03');
        const expected = new Date('2018-09-26');
        const output = TimeUtils.getPreviousWeek(input);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getPreviousWeek(2018-03-07) should return 2018-09-26', () => {
        const input = new Date('2016-03-07');
        const expected = new Date('2016-02-29');
        const output = TimeUtils.getPreviousWeek(input);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getNextWeek
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('TimeUtils.getNextWeek(2018-10-24) should return 2018-10-31', () => {
        const input = new Date('2018-10-24');
        const expected = new Date('2018-10-31');
        const output = TimeUtils.getNextWeek(input);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getNextWeek(2018-12-26) should return 2019-01-02', () => {
        const input = new Date('2018-12-26');
        const expected = new Date('2019-01-02');
        const output = TimeUtils.getNextWeek(input);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    it('TimeUtils.getNextWeek(2016-02-22) should return 2016-02-29', () => {
        const input = new Date('2016-02-22');
        const expected = new Date('2016-02-29');
        const output = TimeUtils.getNextWeek(input);
        expect(output.getFullYear()).toBe(expected.getFullYear());
        expect(output.getMonth()).toBe(expected.getMonth());
        expect(output.getDate()).toBe(expected.getDate());
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getSecondsFromHumanMeasure
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    it('TimeUtils.getSecondsFromHumanMeasure(m) should return 60', () => {
        const expected = TimeEnum.M;
        const output = TimeUtils.getSecondsFromHumanMeasure(TimeEnum.M_ALPHA);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getSecondsFromHumanMeasure(m) should return 3600', () => {
        const expected = TimeEnum.H;
        const output = TimeUtils.getSecondsFromHumanMeasure(TimeEnum.H_ALPHA);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getSecondsFromHumanMeasure(d) should return 28800', () => {
        const expected = TimeEnum.D;
        const output = TimeUtils.getSecondsFromHumanMeasure(TimeEnum.D_ALPHA);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getSecondsFromHumanMeasure(w) should return 144000', () => {
        const expected = TimeEnum.W;
        const output = TimeUtils.getSecondsFromHumanMeasure(TimeEnum.W_ALPHA);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getSecondsFromHumanMeasure(test) should return 0', () => {
        const input = 'test';
        const expected = 0;
        const output = TimeUtils.getSecondsFromHumanMeasure(input);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getSecondsFromHumanMeasure(M) should return 60', () => {
        const input = 'M';
        const expected = TimeEnum.M;
        const output = TimeUtils.getSecondsFromHumanMeasure(input);
        expect(output).toBe(expected);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getHumanTimeToSeconds
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('TimeUtils.getHumanTimeToSeconds(\'10m\') should return 600', () => {
        const input = '10m';
        const expected = 600;
        const output = TimeUtils.getHumanTimeToSeconds(input);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getHumanTimeToSeconds(\'5h 10m\') should return 18600', () => {
        const input = '5h 10m';
        const expected = 18600;
        const output = TimeUtils.getHumanTimeToSeconds(input);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getHumanTimeToSeconds(\'3d 5h 10m\') should return 87000', () => {
        const input = '3d 5h 10m';
        const expected = 105000;
        const output = TimeUtils.getHumanTimeToSeconds(input);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getHumanTimeToSeconds(\'2w 3d 5h 10m\') should return 375000', () => {
        const input = '2W 3d 5h 10m';
        const expected = 393000;
        const output = TimeUtils.getHumanTimeToSeconds(input);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getHumanTimeToSeconds(\'5h 0m\') should return 18000', () => {
        const input = '5h 0m';
        const expected = 18000;
        const output = TimeUtils.getHumanTimeToSeconds(input);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getHumanTimeToSeconds(\'5h test\') should return 18000', () => {
        const input = '5h test';
        const expected = 18000;
        const output = TimeUtils.getHumanTimeToSeconds(input);
        expect(output).toBe(expected);
    });

    it('TimeUtils.getHumanTimeToSeconds(\'5h test\') should return 18000', () => {
        const input = 'added 6h 30m of time spent at 2018-10-16';
        const expected = 23400;
        const output = TimeUtils.getHumanTimeToSeconds(input);
        expect(output).toBe(expected);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getHumanTimeToSeconds
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(EN) should be sunday', () => {
        const date = new Date('2018-10-07');
        expect(TimeUtils.getWeekDayName(date, LocaleEnum.EN).toLowerCase()).toBe('sunday'.toLowerCase());
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(FR) should be dimanche', () => {
        const date = new Date('2018-10-07');
        expect(TimeUtils.getWeekDayName(date, LocaleEnum.FR).toLowerCase()).toBe('dimanche'.toLowerCase());
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(EN) should be monday', () => {
        const date = new Date('2018-10-08');
        expect(TimeUtils.getWeekDayName(date, LocaleEnum.EN).toLowerCase()).toBe('monday'.toLowerCase());
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(FR) should be lundi', () => {
        const date = new Date('2018-10-08');
        expect(TimeUtils.getWeekDayName(date, LocaleEnum.FR).toLowerCase()).toBe('lundi'.toLowerCase());
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(EN) should be wednesday', () => {
        const date = new Date('2018-10-10');
        expect(TimeUtils.getWeekDayName(date, LocaleEnum.EN).toLowerCase()).toBe('wednesday'.toLowerCase());
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(FR) should be mercredi', () => {
        const date = new Date('2018-10-10');
        expect(TimeUtils.getWeekDayName(date, LocaleEnum.FR).toLowerCase()).toBe('mercredi'.toLowerCase());
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(EN) should be saturday', () => {
        const date = new Date('2018-10-13');
        expect(TimeUtils.getWeekDayName(date, LocaleEnum.EN).toLowerCase()).toBe('saturday'.toLowerCase());
    });

    it('MyWorkDay(2018-10-10, logs).getWeekDayName(FR) should be samedi', () => {
        const date = new Date('2018-10-13');
        expect(TimeUtils.getWeekDayName(date, LocaleEnum.FR).toLowerCase()).toBe('samedi'.toLowerCase());
    });

});
