/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {CheckUtils} from './check.utils';

describe('Utils: Check', () => {

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  isEmpty
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('CheckUtils.isEmpty(undefined) should return true', () => {
        const dummyVariable = undefined;
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty(null) should return true', () => {
        const dummyVariable = null;
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty("") should return true', () => {
        const dummyVariable = '';
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty([]) should return true', () => {
        const dummyVariable = [];
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty({}) should return true', () => {
        const dummyVariable = {};
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty(1) should return false', () => {
        const dummyVariable = 1;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty(0) should return false', () => {
        const dummyVariable = 0;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty([0]) should return false', () => {
        const dummyVariable = [0];
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty({test:0}) should return false', () => {
        const dummyVariable = {test: 0};
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isEmpty({test:""}) should return false', () => {
        const dummyVariable = {test: ''};
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isEmpty(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  isNull
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('CheckUtils.isNull(null) should return true', () => {
        const dummyVariable = null;
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isNull(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNull(1) should return false', () => {
        const dummyVariable = 1;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNull(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNull(0) should return false', () => {
        const dummyVariable = 0;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNull(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNull("") should return false', () => {
        const dummyVariable = '';
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNull(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNull([]) should return false', () => {
        const dummyVariable = [];
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNull(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNull({}) should return false', () => {
        const dummyVariable = {};
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNull(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  isNullOrUndefined
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('CheckUtils.isNullOrUndefined(undefined) should return true', () => {
        const dummyVariable = undefined;
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isNullOrUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNullOrUndefined(null) should return false', () => {
        const dummyVariable = null;
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isNullOrUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNullOrUndefined(1) should return false', () => {
        const dummyVariable = 1;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNullOrUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNullOrUndefined(0) should return false', () => {
        const dummyVariable = 0;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNullOrUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNullOrUndefined("") should return false', () => {
        const dummyVariable = '';
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNullOrUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNullOrUndefined([]) should return false', () => {
        const dummyVariable = [];
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNullOrUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isNullOrUndefined({}) should return false', () => {
        const dummyVariable = {};
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isNullOrUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  isObject
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('CheckUtils.isObject(undefined) should return true', () => {
        const dummyVariable = undefined;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isObject(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isObject(null) should return false', () => {
        const dummyVariable = null;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isObject(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isObject(1) should return false', () => {
        const dummyVariable = 1;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isObject(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isObject(0) should return false', () => {
        const dummyVariable = 0;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isObject(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isObject("") should return false', () => {
        const dummyVariable = '';
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isObject(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isObject([]) should return false', () => {
        const dummyVariable = [];
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isObject(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isObject({}) should return false', () => {
        const dummyVariable = {};
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isObject(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  isObject
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('CheckUtils.isString(undefined) should return true', () => {
        const dummyVariable = undefined;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isString(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isString(null) should return false', () => {
        const dummyVariable = null;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isString(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isString(1) should return false', () => {
        const dummyVariable = 1;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isString(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isString(0) should return false', () => {
        const dummyVariable = 0;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isString(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isString("") should return false', () => {
        const dummyVariable = '';
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isString(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isString("test") should return false', () => {
        const dummyVariable = 'test';
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isString(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isString([]) should return false', () => {
        const dummyVariable = [];
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isString(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isString({}) should return false', () => {
        const dummyVariable = {};
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isString(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  isUndefined
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('CheckUtils.isUndefined(undefined) should return true', () => {
        const dummyVariable = undefined;
        const dummyResult = true;
        const dummyRCheck = CheckUtils.isUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isUndefined(null) should return false', () => {
        const dummyVariable = null;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isUndefined(1) should return false', () => {
        const dummyVariable = 1;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isUndefined(0) should return false', () => {
        const dummyVariable = 0;
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isUndefined("") should return false', () => {
        const dummyVariable = '';
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isUndefined([]) should return false', () => {
        const dummyVariable = [];
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });

    it('CheckUtils.isUndefined({}) should return false', () => {
        const dummyVariable = {};
        const dummyResult = false;
        const dummyRCheck = CheckUtils.isUndefined(dummyVariable);
        expect(dummyRCheck).toBe(dummyResult);
    });


});
