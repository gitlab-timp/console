/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {LocaleUtils} from './locale.utils';
import {LocaleEnum} from '../enums/locale.enum';

describe('Utils: Locale', () => {

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getHoursForLocale
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('LocaleUtils.getHoursForLocale() should return h', () => {
        const expectedRes = 'h';
        const res = LocaleUtils.getHoursForLocale();
        expect(res).toBe(expectedRes);
    });

    it('LocaleUtils.getHoursForLocale(EN) should return h', () => {
        const dummyLocale = LocaleEnum.EN;
        const expectedRes = 'h';
        const res = LocaleUtils.getHoursForLocale(dummyLocale);
        expect(res).toBe(expectedRes);
    });

    it('LocaleUtils.getHoursForLocale(FR) should return h', () => {
        const dummyLocale = LocaleEnum.FR;
        const expectedRes = 'h';
        const res = LocaleUtils.getHoursForLocale(dummyLocale);
        expect(res).toBe(expectedRes);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getMinutesForLocale
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('LocaleUtils.getMinutesForLocale() should return h', () => {
        const expectedRes = 'm';
        const res = LocaleUtils.getMinutesForLocale();
        expect(res).toBe(expectedRes);
    });

    it('LocaleUtils.getMinutesForLocale(EN) should return h', () => {
        const dummyLocale = LocaleEnum.EN;
        const expectedRes = 'm';
        const res = LocaleUtils.getMinutesForLocale(dummyLocale);
        expect(res).toBe(expectedRes);
    });

    it('LocaleUtils.getMinutesForLocale(FR) should return h', () => {
        const dummyLocale = LocaleEnum.FR;
        const expectedRes = 'm';
        const res = LocaleUtils.getMinutesForLocale(dummyLocale);
        expect(res).toBe(expectedRes);
    });

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                  getMinutesForLocale
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    it('LocaleUtils.getWeekDaysNames() should return h', () => {
        const expectedRes = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const res = LocaleUtils.getWeekDaysNames();
        expect(res[0]).toBe(expectedRes[0]);
        expect(res[1]).toBe(expectedRes[1]);
        expect(res[2]).toBe(expectedRes[2]);
        expect(res[3]).toBe(expectedRes[3]);
        expect(res[4]).toBe(expectedRes[4]);
        expect(res[5]).toBe(expectedRes[5]);
        expect(res[6]).toBe(expectedRes[6]);
    });

    it('LocaleUtils.getWeekDaysNames(EN) should return h', () => {
        const dummyLocale = LocaleEnum.EN;
        const expectedRes = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const res = LocaleUtils.getWeekDaysNames(dummyLocale);
        expect(res[0]).toBe(expectedRes[0]);
        expect(res[1]).toBe(expectedRes[1]);
        expect(res[2]).toBe(expectedRes[2]);
        expect(res[3]).toBe(expectedRes[3]);
        expect(res[4]).toBe(expectedRes[4]);
        expect(res[5]).toBe(expectedRes[5]);
        expect(res[6]).toBe(expectedRes[6]);
    });

    it('LocaleUtils.getWeekDaysNames(FR) should return h', () => {
        const dummyLocale = LocaleEnum.FR;
        const expectedRes = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
        const res = LocaleUtils.getWeekDaysNames(dummyLocale);
        expect(res[0]).toBe(expectedRes[0]);
        expect(res[1]).toBe(expectedRes[1]);
        expect(res[2]).toBe(expectedRes[2]);
        expect(res[3]).toBe(expectedRes[3]);
        expect(res[4]).toBe(expectedRes[4]);
        expect(res[5]).toBe(expectedRes[5]);
        expect(res[6]).toBe(expectedRes[6]);
    });

});
