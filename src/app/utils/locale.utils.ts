/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {LocaleEnum} from '../enums/locale.enum';
import {CheckUtils} from './check.utils';

export class LocaleUtils {

    /**
     * Return hours mesure locacale printable value
     *
     * @param locale Locale of user
     */
    static getHoursForLocale(locale: LocaleEnum = LocaleEnum.EN): string {
        const localesData: Array<string> = [];
        localesData[LocaleEnum.EN] = 'h';
        localesData[LocaleEnum.FR] = 'h';

        const result = localesData[locale];
        return CheckUtils.isNullOrUndefined(result) ? localesData[LocaleEnum.EN] : result;
    }

    /**
     * Return hours mesure locacale printable value
     *
     * @param locale Locale of user
     */
    static getMinutesForLocale(locale: LocaleEnum = LocaleEnum.EN): string {
        const localesData: Array<string> = [];
        localesData[LocaleEnum.EN] = 'm';
        localesData[LocaleEnum.FR] = 'm';

        const result = localesData[locale];
        return CheckUtils.isNullOrUndefined(result) ? localesData[LocaleEnum.EN] : result;
    }

    /**
     * Get day name for a given locale
     *
     * @param locale Locale of the name
     */
    static getWeekDaysNames(locale: LocaleEnum = LocaleEnum.EN): Array<string> {
        const localesData: Array<Array<string>> = [];
        localesData[LocaleEnum.EN] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        localesData[LocaleEnum.FR] = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

        const result = localesData[locale];
        return CheckUtils.isNullOrUndefined(result) ? localesData[LocaleEnum.EN] : result;
    }

}
