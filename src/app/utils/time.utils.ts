/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {TimeEnum} from '../enums/time.enum';
import {DaysOfWeekEnum} from '../enums/days-of-week.enum';
import {DividerEnum} from '../enums/divider.enum';
import {RegexUtils} from './regex.utils';
import {LocaleEnum} from '../enums/locale.enum';
import {LocaleUtils} from './locale.utils';

/**
 * Will be used to parse and compute times from notes
 */
export class TimeUtils {

    /**
     * Get day name for a given locale
     *
     * @param d Date to get day name
     * @param locale Locale of the name
     */
    static getWeekDayName(d: Date, locale: LocaleEnum = LocaleEnum.EN): string {
        const dayNames = LocaleUtils.getWeekDaysNames(locale);
        return dayNames[d.getDay()];
    }

    /**
     * Get seconds matching to human measure
     *
     * @param humanMeasure String of human measure
     */
    static getSecondsFromHumanMeasure(humanMeasure: string): number {
        const secondsFromHuman = [];
        secondsFromHuman[TimeEnum.M_ALPHA] = TimeEnum.M;
        secondsFromHuman[TimeEnum.H_ALPHA] = TimeEnum.H;
        secondsFromHuman[TimeEnum.D_ALPHA] = TimeEnum.D;
        secondsFromHuman[TimeEnum.W_ALPHA] = TimeEnum.W;
        const seconds = secondsFromHuman[humanMeasure.toLowerCase()];
        return (!isNaN(seconds)) ? seconds : 0;
    }

    /**
     * Convert human readable time to seconds
     *
     * @param humanTime String containing humanreadable time
     */
    static getHumanTimeToSeconds(humanTime: string): number {
        const cleanHumanTime = RegexUtils.extractTimeSpent(humanTime);
        let totalSeconds = 0;

        // check if we have an expected human time format
        if (cleanHumanTime !== null) {
            const hTimes = cleanHumanTime.split(DividerEnum.HUMAN_TIME);

            for (const ht of hTimes) {
                const nbMeasure = Number(RegexUtils.extractOnlyDigits(ht));
                const baseMeasure = TimeUtils.getSecondsFromHumanMeasure(RegexUtils.extractOnlyAlpha(ht));

                // check if we have an expected time measure format
                if (nbMeasure !== null && baseMeasure !== null) {
                    totalSeconds += (nbMeasure * baseMeasure);
                }
            }
        }

        return totalSeconds;
    }

    /**
     * Get date of the date of week
     *
     * @param d Current date
     * @param dow Day of week to getAuthenticated the date
     */
    static getDateForDayOfWeek(d: Date, dow: number): Date {
        const cDay = d.getDay();
        let day = d.getDate();
        dow = (dow === DaysOfWeekEnum.SUNDAY) ? TimeEnum.DAYS_PER_WEEK : dow;

        if (cDay < dow) {
            day += (dow - cDay);
        } else if (cDay > dow) {
            day -= (cDay - dow);
        }

        // if d is a sunday send "previous" week
        if (cDay === DaysOfWeekEnum.SUNDAY) {
            day -= TimeEnum.DAYS_PER_WEEK;
        }

        return new Date(d.getFullYear(), d.getMonth(), day);
    }

    /**
     * Get week number from a date
     *
     * @param d Date to getAuthenticated week number
     */
    static getWeekNumber(d: Date): number {
        const iDate = new Date(d.getFullYear(), 0, 1);
        const deltaTime = (d.getTime() - iDate.getTime());
        const days = (deltaTime / TimeEnum.MILLI_SECONDS_PER_DAY);
        const weekNumber = (Math.ceil((days + iDate.getDay() + 1) / TimeEnum.DAYS_PER_WEEK));
        const checkedWeekNumber = (d.getDay() === DaysOfWeekEnum.SUNDAY) ? (weekNumber - 1) : weekNumber;
        return ((checkedWeekNumber % TimeEnum.WEEKS_PER_YEAR) === 0) ? TimeEnum.WEEKS_PER_YEAR : checkedWeekNumber % TimeEnum.WEEKS_PER_YEAR;
    }

    /**
     * Get next week date from current date
     *
     * @param d Current date
     */
    static getNextWeek(d: Date): Date {
        return new Date(d.getTime() + (TimeEnum.DAYS_PER_WEEK * TimeEnum.MILLI_SECONDS_PER_DAY));
    }

    /**
     * Get previous week date from current date
     *
     * @param d Current Date
     */
    static getPreviousWeek(d: Date): Date {
        return new Date(d.getTime() - (TimeEnum.DAYS_PER_WEEK * TimeEnum.MILLI_SECONDS_PER_DAY));
    }

    /**
     * Check if 2 dates are the same day
     *
     * @param d1 Date to compare
     * @param d2 Date to compare
     */
    static isSameDay(d1: Date, d2: Date): boolean {
        return (d1.getFullYear() === d2.getFullYear()) && (d1.getMonth() === d2.getMonth()) &&
            (d1.getDate() === d2.getDate());
    }

    /**
     * Check if given date is today
     *
     * @param d Date to check
     */
    static isToday(d: Date): boolean {
        const today = new Date();
        return (d.getFullYear() === today.getFullYear()) && (d.getMonth() === today.getMonth()) &&
            (d.getDate() === today.getDate());
    }

    /**
     * Convert seconds to minutes
     *
     * @param seconds Seconds to convert
     */
    static convertSecondsToMinutes(seconds: number): number {
        return (seconds <= 0) ? 0 : Math.round(seconds / TimeEnum.SECONDS_PER_MINUTE);
    }

    /**
     * Convert seconds to hours
     *
     * @param seconds Seconds to convert
     */
    static convertSecondsToHours(seconds: number): number {
        return (seconds <= 0) ? 0 : Math.round(seconds / TimeEnum.SECONDS_PER_HOUR);
    }

}
