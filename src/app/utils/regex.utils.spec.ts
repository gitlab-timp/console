/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {RegexUtils} from './regex.utils';

describe('Utils: Regex', () => {

    it('RegexUtils.extractTimeSpent(added of time spent at 2018-10-16) should return null', () => {
        const dummyNote = 'added of time spent at 2018-10-16';
        const dummyTime = null;
        const dummyRTime = RegexUtils.extractTimeSpent(dummyNote);
        expect(dummyRTime).toBe(dummyTime);
    });

    it('RegexUtils.extractTimeSpent(added 6h 30m of time spent at 2018-10-16) should return 6h 30m', () => {
        const dummyNote = 'added 6h 30m of time spent at 2018-10-16';
        const dummyTime = '6h 30m';
        const dummyRTime = RegexUtils.extractTimeSpent(dummyNote);
        expect(dummyRTime).toBe(dummyTime);
    });

    it('RegexUtils.extractTimeSpent(added 6h 30m of time spent at 2018-10-16) should return 6h 30m', () => {
        const dummyNote = 'added 6h 30m of time spent at 2018-10-16';
        const dummyTime = '6h 30m';
        const dummyRTime = RegexUtils.extractTimeSpent(dummyNote);
        expect(dummyRTime).toBe(dummyTime);
    });

    it('RegexUtils.extractTimeSpent(added 2w 6h 30m of time spent at 2018-10-16) should return 6h 30m', () => {
        const dummyNote = 'added 2w 6h 30m of time spent at 2018-10-16';
        const dummyTime = '2w 6h 30m';
        const dummyRTime = RegexUtils.extractTimeSpent(dummyNote);
        expect(dummyRTime).toBe(dummyTime);
    });

    it('RegexUtils.extractTimeSpent(added 2w of time spent at 2018-10-16) should return 6h 30m', () => {
        const dummyNote = 'added 2w of time spent at 2018-10-16';
        const dummyTime = '2w';
        const dummyRTime = RegexUtils.extractTimeSpent(dummyNote);
        expect(dummyRTime).toBe(dummyTime);
    });

    it('RegexUtils.extractTimeSpent(added 5m of time spent at 2018-10-16) should return 6h 30m', () => {
        const dummyNote = 'added 5m of time spent at 2018-10-16';
        const dummyTime = '5m';
        const dummyRTime = RegexUtils.extractTimeSpent(dummyNote);
        expect(dummyRTime).toBe(dummyTime);
    });

    it('RegexUtils.extractTimeSpent(added 5d 30m of time spent at 2018-10-16) should return 6h 30m', () => {
        const dummyNote = 'added 5d 30m of time spent at 2018-10-16';
        const dummyTime = '5d 30m';
        const dummyRTime = RegexUtils.extractTimeSpent(dummyNote);
        expect(dummyRTime).toBe(dummyTime);
    });

    it('RegexUtils.extractDateTimeSpent(added 5d 30m of time spent at) should return null', () => {
        const dummyNote = 'added 5d 30m of time spent at';
        const dummyTime = null;
        const dummyRTime = RegexUtils.extractDateTimeSpent(dummyNote);
        expect(dummyRTime).toBe(dummyTime);
    });

    it('RegexUtils.extractDateTimeSpent(added 5d 30m of time spent at 2018-10-16) should return 2018-10-16', () => {
        const dummyNote = 'added 5m of time spent at 2018-10-16';
        const dummyDate = new Date('2018-10-16');
        const dummyRDate = RegexUtils.extractDateTimeSpent(dummyNote);
        expect(dummyRDate.getTime()).toBe(dummyDate.getTime());
    });

    it('RegexUtils.extractDateTimeSpent(added 5d 30m of time spent at 0000-00-00) should return new Date(0000-00-00)', () => {
        const dummyNote = 'added 5d 30m of time spent at 0000-00-00';
        const dummyDate = null;
        const dummyRDate = RegexUtils.extractDateTimeSpent(dummyNote);
        expect(dummyRDate).toBe(dummyDate);
    });

    it('RegexUtils.extractOnlyAlpha(5d 30m) should return (d m)', () => {
        const dummyNote = '5d 30m';
        const dummyAlpha = 'd m';
        const dummyRAlpha = RegexUtils.extractOnlyAlpha(dummyNote);
        expect(dummyRAlpha).toBe(dummyAlpha);
    });

    it('RegexUtils.extractOnlyAlpha(5 30) should return null', () => {
        const dummyNote = '5 30';
        const dummyAlpha = null;
        const dummyRAlpha = RegexUtils.extractOnlyAlpha(dummyNote);
        expect(dummyRAlpha).toBe(dummyAlpha);
    });

    it('RegexUtils.extractOnlyDigits(5d 30m) should return (5 30)', () => {
        const dummyNote = '5d 30m';
        const dummyAlpha = '5 30';
        const dummyRAlpha = RegexUtils.extractOnlyDigits(dummyNote);
        expect(dummyRAlpha).toBe(dummyAlpha);
    });

    it('RegexUtils.extractOnlyDigits(d m) should return null', () => {
        const dummyNote = 'd m';
        const dummyAlpha = null;
        const dummyRAlpha = RegexUtils.extractOnlyDigits(dummyNote);
        expect(dummyRAlpha).toBe(dummyAlpha);
    });

});
