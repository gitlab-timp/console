/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {DividerEnum} from '../enums/divider.enum';
import {CheckUtils} from './check.utils';

export class RegexUtils {

    /**
     * Extract note time spent
     *
     * @param noteBody Body of the note
     */
    static extractTimeSpent(noteBody: string): string {
        const eTime = noteBody.match(/(([\d]{1,2}[Ww]{1}\s*)*|([\d]{1,2}[Dd]{1}\s*)*|([\d]{1,2}[Hh]{1}\s*)*|([\d]{1,2}[Mm]{1}\s*)*)+/gim);

        if (!CheckUtils.isNullOrUndefined(eTime)) {
            const result = eTime.join(DividerEnum.HUMAN_TIME).trim();

            if (!CheckUtils.isEmpty(result)) {
                return result;
            }
        }

        return null;
    }

    /**
     * Extract note date of time spent
     *
     * @param noteBody Body of the note
     */
    static extractDateTimeSpent(noteBody: string): Date {
        const eStrDate = noteBody.match(/([\d]{4}-[\d]{2}-[\d]{2})+/gim);
        const eDate = !CheckUtils.isNullOrUndefined(eStrDate) ? new Date(eStrDate[0]) : null;
        return (!CheckUtils.isNullOrUndefined(eDate) && !isNaN(eDate.getTime())) ? eDate : null;
    }

    /**
     * Extract only alpha chars from text
     *
     * @param text Text to check
     */
    static extractOnlyAlpha(text: string): string {
        const alpha = text.match(/[a-zA-Z]+/gim);
        return !CheckUtils.isNullOrUndefined(alpha) ? alpha.join(DividerEnum.ALPHAS).trim() : null;
    }

    /**
     * Extract only digits chars from text
     *
     * @param text Text to check
     */
    static extractOnlyDigits(text: string): string {
        const digits = text.match(/\d+/gim);
        return !CheckUtils.isNullOrUndefined(digits) ? digits.join(DividerEnum.DIGITS).trim() : null;
    }

    /**
     * Check if note is a added time log note
     *
     * @param noteBody Body of the note
     */
    static isAddTimeLogNote(noteBody: string): boolean {
        const body = noteBody.match(/^(added)\s*(([\d]{1,2}[Ww]{1}\s*)*|([\d]{1,2}[Dd]{1}\s*)*|([\d]{1,2}[Hh]{1}\s*)*|([\d]{1,2}[Mm]{1}\s*)*)+\s*(of\s*time\s*spent\s*at)\s*([\d]{4}-[\d]{2}-[\d]{2})+/gim);
        return !CheckUtils.isNullOrUndefined(body);
    }

    /**
     * Check if note is a added time log note
     *
     * @param noteBody Body of the note
     */
    static isSubtractedTimeLogNote(noteBody: string): boolean {
        const body = noteBody.match(/^(subtracted)\s*(([\d]{1,2}[Ww]{1}\s*)*|([\d]{1,2}[Dd]{1}\s*)*|([\d]{1,2}[Hh]{1}\s*)*|([\d]{1,2}[Mm]{1}\s*)*)+\s*(of\s*time\s*spent\s*at)\s*([\d]{4}-[\d]{2}-[\d]{2})+/gim);
        return !CheckUtils.isNullOrUndefined(body);
    }

}
