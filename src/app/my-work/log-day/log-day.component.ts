/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, Input, OnInit} from '@angular/core';
import {MyWorkDay} from '../../model/my-work-day';
import {ColorEnum} from '../../enums/color.enum';
import {CheckUtils} from '../../utils/check.utils';

@Component({
    selector: 'app-log-day',
    templateUrl: './log-day.component.html',
    styleUrls: ['./log-day.component.css']
})

export class LogDayComponent implements OnInit {

    @Input() data: MyWorkDay;

    public cardBackgroundColor: string;
    public humanReadableTotal: string;
    public weekDayName: string;

    constructor() {
        this.data = null;
        this.humanReadableTotal = '';
        this.cardBackgroundColor = ColorEnum.DEFAULT;
        this.weekDayName = '';
    }

    ngOnInit() {
        // check data have been set
        if (!CheckUtils.isNullOrUndefined(this.data)) {
            this.humanReadableTotal = this.data.getHumanReadableTimeTotal().getPrintableTotal();
            this.cardBackgroundColor = this.data.getBackgroundColor();
            this.weekDayName = this.data.getWeekDayName();
        }
    }

}
