/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LogDayComponent} from './log-day.component';
import {LogEntryModule} from '../log-entry/log-entry.module';
import {NoteableTypeEnum} from '../../enums/noteable-type.enum';
import {TimeLog} from '../../model/time-log';
import {MyWorkDay} from '../../model/my-work-day';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../material.module';

describe('Component: LogDay', () => {
    let component: LogDayComponent;
    let fixture: ComponentFixture<LogDayComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                LogDayComponent
            ],
            imports: [
                CommonModule,
                LogEntryModule,
                MaterialModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LogDayComponent);
        component = fixture.componentInstance;
        component.data = new MyWorkDay(
            new Date(),
            [
                new TimeLog(
                    1,
                    1,
                    'Test',
                    new Date().toISOString(),
                    1,
                    'Test/Test',
                    1,
                    NoteableTypeEnum.ISSUE,
                    'Test',
                    3000,
                    new Date(),
                    false,
                    []
                )
            ]
        );

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
