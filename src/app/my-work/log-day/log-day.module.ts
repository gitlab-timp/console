/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LogDayComponent} from './log-day.component';
import {LogEntryModule} from '../log-entry/log-entry.module';
import {MaterialModule} from '../../material.module';

@NgModule({
    declarations: [
        LogDayComponent
    ],
    exports: [
        LogDayComponent
    ],
    imports: [
        CommonModule,
        LogEntryModule,
        MaterialModule
    ]
})

export class LogDayModule {
}
