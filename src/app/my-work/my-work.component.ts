/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IssuesService} from '../service/git-lab/issues/issues.service';
import {ProjectsService} from '../service/git-lab/projects/projects.service';
import {MergeRequestsService} from '../service/git-lab/merge-requests/merge-requests.service';
import {UsersService} from '../service/git-lab/users/users.service';

import {ColorEnum} from '../enums/color.enum';
import {DaysOfWeekEnum} from '../enums/days-of-week.enum';
import {LoadingStatusEnum} from '../enums/loading-status.enum';

import {CheckUtils} from '../utils/check.utils';
import {TimeUtils} from '../utils/time.utils';
import {User} from '../entity/user';

import {LegendEntry} from '../model/legend-entry';
import {LoadingStep} from '../model/loading-step';
import {MyWorkDay} from '../model/my-work-day';
import {TimeLog} from '../model/time-log';
import {RawData} from '../model/raw-data';
import {NotesService} from '../service/git-lab/notes/notes.service';
import {NoteableTypeEnum} from '../enums/noteable-type.enum';
import {WorkWeek} from '../model/work-week';


@Component({
    moduleId: module.id,
    selector: 'app-my-work',
    styleUrls: ['./my-work.component.css'],
    templateUrl: './my-work.component.html'
})

export class MyWorkComponent implements OnInit {
    public currentDate: Date;
    public legend: Array<LegendEntry>;
    public isLoadingData: boolean;
    public isLoadingDataError: boolean;
    public loadingSteps: Array<LoadingStep>;
    public rawData: RawData;
    public timeLogs: Array<TimeLog>;
    public workWeek: WorkWeek;
    public myWorkDays: Array<MyWorkDay>;

    constructor(
        private issueService: IssuesService,
        private mergeRequestsService: MergeRequestsService,
        private notesService: NotesService,
        private projectsService: ProjectsService,
        private usersService: UsersService
    ) {
        this.init();
    }

    ngOnInit() {
        this.init();
        this.updateMyWorkLogs();
        this.loadData();
    }

    /**
     * Set inital value for all data
     */
    private init() {
        this.legend = [
            new LegendEntry(NoteableTypeEnum.ISSUE, ColorEnum.ISSUE),
            new LegendEntry(NoteableTypeEnum.MERGE_REQUEST, ColorEnum.MERGE_REQUEST)
        ];

        this.rawData = new RawData();
        this.timeLogs = [];
        this.currentDate = new Date();
        this.workWeek = WorkWeek.createForDate(this.currentDate);
        this.isLoadingData = false;
        this.isLoadingDataError = false;
        this.resetDatas();
    }

    /**
     * Reset loading steps
     */
    private resetLoadingSteps() {
        this.loadingSteps = [
            new LoadingStep(LoadingStatusEnum.FETCHING_PROJECTS),
            new LoadingStep(LoadingStatusEnum.FETCHING_ISSUES),
            new LoadingStep(LoadingStatusEnum.FETCHING_ISSUES_NOTES),
            new LoadingStep(LoadingStatusEnum.FETCHING_MERGE_REQUESTS),
            new LoadingStep(LoadingStatusEnum.FETCHING_MERGE_REQUESTS_NOTES),
            new LoadingStep(LoadingStatusEnum.COMPUTING_LOGS)
        ];
    }

    /**
     * Update loading step done statusMsg
     *
     * @param status Name of the statusMsg
     */
    private loadingStepDone(status: LoadingStatusEnum) {
        const loadingStep = this.loadingSteps.filter((ls: LoadingStep) => {
            return ls.statusMsg === status;
        });

        loadingStep[0].setDone();
    }

    /**
     * Update loading step done statusMsg
     *
     * @param status Name of the statusMsg
     */
    private loadingStepError(status: LoadingStatusEnum) {
        const loadingStep = this.loadingSteps.filter((ls: LoadingStep) => {
            return ls.statusMsg === status;
        });

        loadingStep[0].setError();
    }

    /**
     * Update loading step done statusMsg
     *
     * @param status Name of the statusMsg
     */
    private loadingStepLoading(status: LoadingStatusEnum) {
        const loadingStep = this.loadingSteps.filter((ls: LoadingStep) => {
            return ls.statusMsg === status;
        });

        loadingStep[0].setLoading();
    }

    /**
     * Reset all arrays datas
     */
    private resetDatas() {
        this.isLoadingData = false;
        this.isLoadingDataError = false;
        this.rawData = new RawData();
        this.resetLoadingSteps();
    }

    /**
     * Load all datas to extract time logged
     */
    public loadData() {
        this.resetDatas();
        this.isLoadingData = true;

        this.loadingStepLoading(LoadingStatusEnum.FETCHING_PROJECTS);

        this.projectsService.getAllProjectsPages(this.rawData).subscribe(
            () => {
            },
            () => {
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_PROJECTS);
            },
            () => {
                this.loadingStepDone(LoadingStatusEnum.FETCHING_PROJECTS);
                this.loadIssues();
            }
        );
    }

    /**
     * Load all issues for all projects
     */
    public loadIssues() {
        this.loadingStepLoading(LoadingStatusEnum.FETCHING_ISSUES);
        const jobs: Array<Observable<any>> = [];

        // construct list of queries
        for (const obj of this.rawData.projects) {
            jobs.push(this.issueService.getAllIssuesByProject(this.rawData, obj.id));
        }

        Observable.forkJoin(jobs).subscribe(
            () => {
            },
            () => {
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_ISSUES);
            },
            () => {
                this.loadingStepDone(LoadingStatusEnum.FETCHING_ISSUES);
                this.loadIssuesNotes();
            }
        );
    }

    /**
     * Load all notes for all issues
     */
    public loadIssuesNotes() {
        this.loadingStepLoading(LoadingStatusEnum.FETCHING_ISSUES_NOTES);

        const jobs: Array<Observable<any>> = [];

        // construct list of queries
        for (const obj of this.rawData.issues) {
            jobs.push(this.notesService.loadIssuesNotesPages(this.rawData, obj.project_id, obj.iid));
        }

        Observable.forkJoin(jobs).subscribe(
            () => {
            },
            () => {
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_ISSUES_NOTES);
            },
            () => {
                this.loadingStepDone(LoadingStatusEnum.FETCHING_ISSUES_NOTES);
                this.loadMergeRequests();
            }
        );
    }

    /**
     * Load all merge requests for all projects
     */
    public loadMergeRequests() {
        this.loadingStepLoading(LoadingStatusEnum.FETCHING_MERGE_REQUESTS);
        const jobs: Array<Observable<any>> = [];

        // construct list of queries
        // for (const obj of this.rawData.projects) {
        //    jobs.push(this.mergeRequestsService.getAllMergeRequestsByProject(this.rawData, obj.id));
        // }

        Observable.forkJoin(jobs).subscribe(
            () => {
            },
            () => {
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_MERGE_REQUESTS);
            },
            () => {
                this.loadingStepDone(LoadingStatusEnum.FETCHING_MERGE_REQUESTS);
                this.loadMergeRequestNotes();
            }
        );
    }

    /**
     * Load all notes for all merge requests
     */
    public loadMergeRequestNotes() {
        this.loadingStepLoading(LoadingStatusEnum.FETCHING_MERGE_REQUESTS_NOTES);
        const jobs: Array<Observable<any>> = [];

        // construct list of queries
        // for (const obj of this.rawData.mergeRequests) {
        //     jobs.push(this.notesService.loadMergeRequestNotesPages(this.rawData, obj.noteable_id, obj.iid));
        // }

        Observable.forkJoin(jobs).subscribe(
            () => {
            },
            () => {
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.FETCHING_MERGE_REQUESTS_NOTES);
            },
            () => {
                this.loadingStepDone(LoadingStatusEnum.FETCHING_MERGE_REQUESTS_NOTES);
                this.computeLogs();
            }
        );
    }

    /**
     * Compute times logged
     */
    public computeLogs() {
        this.loadingStepLoading(LoadingStatusEnum.COMPUTING_LOGS);

        this.usersService.getAuthenticated().subscribe(
            (res: User) => {
                this.timeLogs = this.rawData.getTimeLogsForUser(res);
                this.updateMyWorkLogs();
            },
            () => {
                this.isLoadingData = false;
                this.isLoadingDataError = true;
                this.loadingStepError(LoadingStatusEnum.COMPUTING_LOGS);
            },
            () => {
                this.loadingStepDone(LoadingStatusEnum.COMPUTING_LOGS);
                this.isLoadingData = false;
            }
        );
    }

    /**
     * Set logs for current week
     */
    public updateMyWorkLogs() {
        const dateMonday = TimeUtils.getDateForDayOfWeek(this.currentDate, DaysOfWeekEnum.MONDAY);
        const dateTuesday = TimeUtils.getDateForDayOfWeek(this.currentDate, DaysOfWeekEnum.TUESDAY);
        const dateWednesday = TimeUtils.getDateForDayOfWeek(this.currentDate, DaysOfWeekEnum.WEDNESDAY);
        const dateThursday = TimeUtils.getDateForDayOfWeek(this.currentDate, DaysOfWeekEnum.THURSDAY);
        const dateFriday = TimeUtils.getDateForDayOfWeek(this.currentDate, DaysOfWeekEnum.FRIDAY);
        const dateSaturday = TimeUtils.getDateForDayOfWeek(this.currentDate, DaysOfWeekEnum.SATURDAY);
        const dateSunday = TimeUtils.getDateForDayOfWeek(this.currentDate, DaysOfWeekEnum.SUNDAY);

        this.myWorkDays = [
            new MyWorkDay(dateMonday, this.getTimeLogsByDate(dateMonday)),
            new MyWorkDay(dateTuesday, this.getTimeLogsByDate(dateTuesday)),
            new MyWorkDay(dateWednesday, this.getTimeLogsByDate(dateWednesday)),
            new MyWorkDay(dateThursday, this.getTimeLogsByDate(dateThursday)),
            new MyWorkDay(dateFriday, this.getTimeLogsByDate(dateFriday)),
            new MyWorkDay(dateSaturday, this.getTimeLogsByDate(dateSaturday)),
            new MyWorkDay(dateSunday, this.getTimeLogsByDate(dateSunday))
        ];
    }

    /**
     * Get time logs by date
     *
     * @param d Date of looking time logs
     */
    private getTimeLogsByDate(d: Date): Array<TimeLog> {
        const logsOf: Array<TimeLog> = this.timeLogs.filter((tl: TimeLog) => {
            return TimeUtils.isSameDay(tl.date_of_log, d);
        });

        return !CheckUtils.isNullOrUndefined(logsOf) ? logsOf : [];
    }

    /**
     * Display next week work log
     */
    public nextWeek() {
        this.currentDate = TimeUtils.getNextWeek(this.currentDate);
        this.workWeek = WorkWeek.createForDate(this.currentDate);
        this.updateMyWorkLogs();
    }

    /**
     * Display previous week work log
     */
    public previousWeek() {
        this.currentDate = TimeUtils.getPreviousWeek(this.currentDate);
        this.workWeek = WorkWeek.createForDate(this.currentDate);
        this.updateMyWorkLogs();
    }

}
