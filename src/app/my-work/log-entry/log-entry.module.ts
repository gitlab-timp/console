/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LogEntryComponent} from './log-entry.component';
import {MaterialModule} from '../../material.module';

@NgModule({
    declarations: [
        LogEntryComponent
    ],
    exports: [
        LogEntryComponent
    ],
    imports: [
        CommonModule,
        MaterialModule
    ]
})

export class LogEntryModule {
}
