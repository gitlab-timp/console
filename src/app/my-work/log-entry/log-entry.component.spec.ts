/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LogEntryComponent} from './log-entry.component';
import {NoteableTypeEnum} from '../../enums/noteable-type.enum';
import {TimeLog} from '../../model/time-log';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../material.module';

describe('Component: LogEntry', () => {
    let component: LogEntryComponent;
    let fixture: ComponentFixture<LogEntryComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                LogEntryComponent
            ],
            imports: [
                CommonModule,
                MaterialModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LogEntryComponent);
        component = fixture.componentInstance;
        component.data = new TimeLog(
            1,
            1,
            'Test',
            new Date().toISOString(),
            1,
            'Test/Test',
            1,
            NoteableTypeEnum.ISSUE,
            'Test',
            3000,
            new Date(),
            false,
            []
        );

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
