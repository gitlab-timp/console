/*
 * This file is part of the Timp software.
 *
 * (c) Timp <https://gitlab.com/gitlab-timp>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, Input, OnInit} from '@angular/core';
import {TimeLog} from '../../model/time-log';
import {ColorEnum} from '../../enums/color.enum';
import {CheckUtils} from '../../utils/check.utils';

@Component({
    selector: 'app-log-entry',
    styleUrls: ['./log-entry.component.css'],
    templateUrl: './log-entry.component.html'
})
export class LogEntryComponent implements OnInit {

    @Input() data: TimeLog;

    public cardBackgroundColor: string;
    public humanReadableTime: string;
    public iidBackgroundColor: string;

    constructor() {
        this.data = null;
        this.cardBackgroundColor = ColorEnum.LOG_ENTRY_DEFAULT;
        this.humanReadableTime = '';
        this.iidBackgroundColor = ColorEnum.IID_DEFAULT;
    }

    ngOnInit() {
        // check data have been set
        if (!CheckUtils.isNullOrUndefined(this.data)) {
            this.cardBackgroundColor = this.data.getNoteableColor();
            this.humanReadableTime = this.data.getHumanReadableTime().getPrintable();
            this.iidBackgroundColor = this.data.getIIDColor();
        }
    }

}
